import { Result } from '@ethersproject/abi';
import { AxiosHeaders, HeadersDefaults, RawAxiosRequestHeaders } from 'axios';
import { BytesLike, TypedDataDomain, TypedDataField } from 'ethers';
import { HttpProvider } from './lib';
import { TronDelegatedResource, TronPermission, TronPermissionPermissionType, TronTransaction } from './proto';

export interface MethodDefaultOptions {
  feeLimit: number;
  callValue: number;
  userFeePercentage: number;
  shouldPollResponse: boolean;
}

export interface MethodExtensionOptions extends MethodDefaultOptions {
  data?: string;
  txLocal?: boolean;
  confirmed?: boolean;
}

export interface MethodCallOptions extends Partial<MethodExtensionOptions> {
  from?: string;
  _isConstant?: boolean;
}

export interface MethodSendOptions extends Partial<MethodExtensionOptions> {
  tokenId?: string;
  tokenValue?: number | string;
  rawResponse?: boolean;
  keepTxID?: boolean;
  estimateEnergy?: boolean;
  permissionId?: number;
}

export interface DeployConstantContractOptions extends ConfirmeOptions {
  input: string;
  ownerAddress: string;
  tokenId?: number;
  tokenValue?: number;
  callValue?: number;
}

//  see: https://cn.developers.tron.network/reference/geteventresult
export interface EventOptions {
  sinceTimestamp?: number;
  //  since?: number;
  //  fromTimestamp?: number;
  eventName?: string;
  blockNumber?: number;
  size?: number;
  page?: number;
  onlyConfirmed?: boolean;
  onlyUnconfirmed?: boolean;
  //  previousLastEventFingerprint?: string;
  //  previousFingerprint?: string;
  fingerprint?: string;
  //  rawResponse?: boolean;
  sort?: 'block_timestamp' | '-block_timestamp' | string;
  filters?: Record<string, any>;
  resourceNode?: string;
}

export interface EventRawOptions extends EventOptions {
  rawResponse: true;
}

export interface EventResult<T extends Record<string, string>> {
  block: number;
  timestamp: number;
  contract: string;
  name: string;
  transaction: string;
  result: T;
  resourceNode: string;
  unconfirmed?: boolean;
  fingerprint?: string;
}

export interface EventRawResult<T extends Result> {
  block_number: number;
  block_timestamp: number;
  contract_address: string;
  event_name: string;
  transaction_id: string;
  result: T;
  resource_Node?: string;
  _unconfirmed?: boolean;
  _fingerprint?: string;

  caller_contract_address: string;
  event: string;
  event_index: number;
  result_type: Record<keyof T, string>;
}

export interface MethodObject {
  call<T = any>(options?: MethodCallOptions): Promise<T>;

  send<T = any>(options?: MethodSendOptions): Promise<T>;

  watch<T = any>(
    options: EventRawOptions,
    callback?: (error: any, event?: EventRawResult<T>) => any
  ): Promise<{ start(): void; stop(): void }>;

  watch<T = any>(
    options?: EventOptions,
    callback?: (error: any, event?: EventResult<T>) => any
  ): Promise<{ start(): void; stop(): void }>;
}

export interface ContractBoundMethod {
  (...args: any[]): MethodObject;
}

//  https://cn.developers.tron.network/reference/tronweb-createsmartcontract
export interface ContractCreateOptions extends MethodSendOptions {
  originEnergyLimit?: number;
  abi?: string;
  bytecode?: string;
  parameters?: string;
  name?: string;
}

export interface ContractEventCallback {
  <T>(event: T): any;
}

export interface ContractEventStartCallback {
  <T>(error?: any): any;
}

export interface ContractEventResult<T> {
  start(startCallback?: ContractEventStartCallback): void;
  stop(): void;
}

export type HttpProviderHeaders = RawAxiosRequestHeaders | AxiosHeaders | Partial<HeadersDefaults>;

export interface PluginOptions extends Record<string, any> {
  disablePlugins?: string[];
}

export interface PluginRegisterResult {
  libs: string[];
  plugged: string[];
  skipped: string[];
}

export interface PluginInterface {
  requires: string;
  components: object;
  //  fullClass?: string;
}

export interface SideChainOptions {
  fullNode?: HttpProvider | string;
  fullHost?: HttpProvider | string;
  solidityNode?: HttpProvider | string;
  eventServer?: HttpProvider | string;
  mainGatewayAddress: string;
  sideGatewayAddress: string;
  sideChainId: string;
}

export interface ValidatorParameter {
  name: string;
  names: string[];
  value: any;
  type: string;
  gt: number;
  lt: number;
  gte: number;
  lte: number;
  se: any;
  optional: boolean;
  msg?: string;
}

export type TronWebProvider = HttpProvider | string;
export type BlockID = 'latest' | 'earliest' | number;

export interface TronWebOptions {
  fullNode?: TronWebProvider;
  fullHost?: TronWebProvider;
  solidityNode?: TronWebProvider;
  eventServer?: TronWebProvider;
  headers?: HttpProviderHeaders;
  eventHeaders?: HttpProviderHeaders;
  privateKey?: string;
}

export interface TronWebNodeServers {
  fullNode: HttpProvider;
  solidityNode: HttpProvider;
  eventServer: HttpProvider;
}

export interface Account {
  privateKey: string;
  publicKey: string;
  address: { base58: string; hex: string };
}

export interface AccountWithMnemonic extends Omit<Account, 'address'> {
  mnemonic: string;
  address: string;
}

export interface AccountOptions {
  path?: string;
  locale?: string;
  extraEntropy?: BytesLike;
}

export interface TypedDataTypedTypes extends Record<string, TypedDataField[]> {}

export interface TypedDataTypedValue extends Record<string, any> {}

export interface TypedDataTypedEncoder {
  (value: TypedDataTypedValue): string;
}

export interface TypedDataVisitCallback {
  (types: TypedDataTypedTypes, value: TypedDataTypedValue): string;
}

export interface TypedDataPayload {
  types: TypedDataTypedTypes;
  domain: TypedDataDomain;
  primaryType: string;
  message: string;
}

export interface TransactionOptions {
  permissionId?: number;
}

export type TransactionResource = 'BANDWIDTH' | 'ENERGY';

export interface UpdateTokenOptions {
  description: string;
  url: string;
  freeBandwidth?: number;
  freeBandwidthLimit?: number;
  permissionId?: number;
}

export interface CreateTokenOptions extends UpdateTokenOptions {
  name: string;
  abbreviation: string;
  totalSupply: number;
  trxRatio?: number;
  tokenRatio?: number;
  saleStart: number;
  saleEnd: number;
  frozenAmount?: number;
  frozenDuration?: number;
  voteScore: number;
  precision: number;
}

export interface Permissions extends TronPermission {
  type: TronPermissionPermissionType;
}

export interface NewTransactionIdentityOptions {
  txLocal?: boolean;
}

export interface AlterTransactionOptions extends NewTransactionIdentityOptions {
  data?: string;
  dataFormat?: TransactionDataFormat;
  extension?: number;
}

export type TransactionDataFormat = 'hex' | 'utf8';

export interface SendTransactionResult {
  result: boolean;
  transaction: TronTransaction;
}

export interface SendHexTransactionResult extends SendTransactionResult {
  hexTransaction: string;
}

export interface SendTransactionOptions {
  privateKey?: string;
  address?: string;
}

export interface ConfirmeOptions {
  confirmed?: boolean;
}

export interface GetDelegatedResourceResult {
  delegatedResource: TronDelegatedResource[];
}
