import { Any } from '../../../protocol/google/protobuf/any';

import { RequiredPick } from '../../../utility';

interface AnyInterface extends Any {}
export interface TronAny<T = any> extends RequiredPick<AnyInterface, 'type_url'> {
  value: T;
}
