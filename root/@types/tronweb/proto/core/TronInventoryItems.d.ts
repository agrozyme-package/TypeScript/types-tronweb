import { InventoryItems } from '../../protocol/core/TronInventoryItems';

export interface TronInventoryItems extends InventoryItems {}
