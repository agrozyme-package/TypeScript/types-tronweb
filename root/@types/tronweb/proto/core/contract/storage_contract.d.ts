import {
  BuyStorageBytesContract,
  BuyStorageContract,
  SellStorageContract,
  UpdateBrokerageContract,
} from '../../../protocol/core/contract/storage_contract';

export interface TronBuyStorageBytesContract extends BuyStorageBytesContract {}

export interface TronBuyStorageContract extends BuyStorageContract {}

export interface TronSellStorageContract extends SellStorageContract {}

export interface TronUpdateBrokerageContract extends UpdateBrokerageContract {}
