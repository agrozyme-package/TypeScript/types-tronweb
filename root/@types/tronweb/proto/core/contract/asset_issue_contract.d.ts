import {
  AssetIssueContract,
  AssetIssueContract_FrozenSupply,
  ParticipateAssetIssueContract,
  TransferAssetContract,
  UnfreezeAssetContract,
  UpdateAssetContract,
} from '../../../protocol/core/contract/asset_issue_contract';

import { RequiredPick } from '../../../utility';

interface AssetIssueContractInterface extends AssetIssueContract {
  frozen_supply?: TronAssetIssueContract_FrozenSupply[];
}
export interface TronAssetIssueContract
  extends RequiredPick<
    AssetIssueContractInterface,
    | 'id'
    | 'owner_address'
    | 'name'
    | 'abbr'
    | 'total_supply'
    | 'trx_num'
    | 'num'
    | 'start_time'
    | 'end_time'
    | 'description'
    | 'url'
  > {}

export interface TronAssetIssueContractFrozenSupply extends AssetIssueContract_FrozenSupply {}

export interface TronTransferAssetContract extends TransferAssetContract {}

export interface TronUnfreezeAssetContract extends UnfreezeAssetContract {}

export interface TronUpdateAssetContract extends UpdateAssetContract {}

export interface TronParticipateAssetIssueContract extends ParticipateAssetIssueContract {}
