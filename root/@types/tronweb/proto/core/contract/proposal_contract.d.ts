import {
  ProposalApproveContract,
  ProposalCreateContract,
  ProposalCreateContract_ParametersEntry,
  ProposalDeleteContract,
} from '../../../protocol/core/contract/proposal_contract';

export interface TronProposalApproveContract extends ProposalApproveContract {}

export interface TronProposalCreateContract extends ProposalCreateContract {}

export interface TronProposalCreateContractParametersEntry extends ProposalCreateContract_ParametersEntry {}

export interface TronProposalDeleteContract extends ProposalDeleteContract {}
