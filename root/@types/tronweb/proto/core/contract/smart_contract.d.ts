import {
  ClearABIContract,
  ContractState,
  CreateSmartContract,
  SmartContract,
  SmartContract_ABI,
  SmartContract_ABI_Entry,
  SmartContract_ABI_Entry_EntryType,
  SmartContract_ABI_Entry_Param,
  SmartContract_ABI_Entry_StateMutabilityType,
  SmartContractDataWrapper,
  TriggerSmartContract,
  UpdateEnergyLimitContract,
  UpdateSettingContract,
} from '../../../protocol/core/contract/smart_contract';

import { RequiredPick } from '../../../utility';

interface SmartContractInterface extends SmartContract {
  abi?: TronSmartContractAbi;
}
export interface TronSmartContract
  extends RequiredPick<
    SmartContractInterface,
    | 'origin_address'
    | 'contract_address'
    | 'abi'
    | 'bytecode'
    | 'consume_user_resource_percent'
    | 'name'
    | 'origin_energy_limit'
    | 'code_hash'
  > {}

export interface TronSmartContractAbi extends SmartContract_ABI {
  entrys: TronSmartContractAbiEntry[];
}

interface SmartContractAbiEntryInterface extends SmartContract_ABI_Entry {
  inputs?: TronSmartContractAbiEntryParameter[];
  outputs?: TronSmartContractAbiEntryParameter[];
  type?: TronSmartContractAbiEntryEntryTypeKey;
  stateMutability?: TronSmartContractAbiEntryStateMutabilityTypeKey;
}
export interface TronSmartContractAbiEntry extends RequiredPick<SmartContractAbiEntryInterface, 'name' | 'type'> {}

export type TronSmartContractAbiEntryEntryType = SmartContract_ABI_Entry_EntryType;
export type TronSmartContractAbiEntryEntryTypeKey = keyof typeof SmartContract_ABI_Entry_EntryType;

export type TronSmartContractAbiEntryStateMutabilityType = SmartContract_ABI_Entry_StateMutabilityType;
export type TronSmartContractAbiEntryStateMutabilityTypeKey = keyof typeof SmartContract_ABI_Entry_StateMutabilityType;

interface SmartContractAbiEntryParameterInterface extends SmartContract_ABI_Entry_Param {}
export interface TronSmartContractAbiEntryParameter
  extends RequiredPick<SmartContractAbiEntryParameterInterface, 'type'> {}

export interface TronContractState extends ContractState {}

export interface TronCreateSmartContract extends CreateSmartContract {
  new_contract?: TronSmartContract;
}

export interface TronTriggerSmartContract extends TriggerSmartContract {}

export interface TronClearAbiContract extends ClearABIContract {}

export interface TronUpdateSettingContract extends UpdateSettingContract {}

export interface TronUpdateEnergyLimitContract extends UpdateEnergyLimitContract {}

export interface TronSmartContractDataWrapper extends SmartContractDataWrapper {
  smart_contract?: TronSmartContract;
  contract_state?: TronContractState;
}
