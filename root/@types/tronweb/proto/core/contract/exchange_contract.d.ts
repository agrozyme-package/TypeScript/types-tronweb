import {
  ExchangeCreateContract,
  ExchangeInjectContract,
  ExchangeTransactionContract,
  ExchangeWithdrawContract,
} from '../../../protocol/core/contract/exchange_contract';

export interface TronExchangeCreateContract extends ExchangeCreateContract {}

export interface TronExchangeInjectContract extends ExchangeInjectContract {}

export interface TronExchangeWithdrawContract extends ExchangeWithdrawContract {}

export interface TronExchangeTransactionContract extends ExchangeTransactionContract {}
