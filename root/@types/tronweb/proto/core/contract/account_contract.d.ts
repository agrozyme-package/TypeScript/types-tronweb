import {
  AccountCreateContract,
  AccountPermissionUpdateContract,
  AccountUpdateContract,
  SetAccountIdContract,
} from '../../../protocol/core/contract/account_contract';
import { TronAccountTypeKey, TronPermission } from '../Tron';

export interface TronAccountCreateContract extends AccountCreateContract {
  type?: TronAccountTypeKey;
}

export interface TronAccountUpdateContract extends AccountUpdateContract {}

export interface TronSetAccountIdContract extends SetAccountIdContract {}

export interface TronAccountPermissionUpdateContractInterface extends AccountPermissionUpdateContract {
  owner?: TronPermission;
  witness?: TronPermission;
  actives?: TronPermission[];
}
