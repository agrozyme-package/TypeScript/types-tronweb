import { VoteAssetContract } from '../../../protocol/core/contract/vote_asset_contract';

export interface TronVoteAssetContract extends VoteAssetContract {}
