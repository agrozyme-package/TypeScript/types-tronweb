import { MarketCancelOrderContract, MarketSellAssetContract } from '../../../protocol/core/contract/market_contract';

export interface TronMarketSellAssetContract extends MarketSellAssetContract {}

export interface TronMarketCancelOrderContract extends MarketCancelOrderContract {}
