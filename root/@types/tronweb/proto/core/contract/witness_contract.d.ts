import {
  VoteWitnessContract,
  VoteWitnessContract_Vote,
  WitnessCreateContract,
  WitnessUpdateContract,
} from '../../../protocol/core/contract/witness_contract';

export interface TronWitnessCreateContract extends WitnessCreateContract {}

export interface TronWitnessUpdateContract extends WitnessUpdateContract {}

export interface TronVoteWitnessContract extends VoteWitnessContract {
  votes?: TronVoteWitnessContractVote[];
}

export interface TronVoteWitnessContractVote extends VoteWitnessContract_Vote {}
