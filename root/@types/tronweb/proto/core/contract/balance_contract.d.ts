import {
  AccountBalanceRequest,
  AccountBalanceResponse,
  AccountIdentifier,
  AccountTrace,
  BlockBalanceTrace,
  BlockBalanceTrace_BlockIdentifier,
  DelegateResourceContract,
  FreezeBalanceContract,
  FreezeBalanceV2Contract,
  TransactionBalanceTrace,
  TransactionBalanceTrace_Operation,
  TransferContract,
  UnDelegateResourceContract,
  UnfreezeBalanceContract,
  UnfreezeBalanceV2Contract,
  WithdrawBalanceContract,
  WithdrawExpireUnfreezeContract,
} from '../../../protocol/core/contract/balance_contract';
import { TronResourceCodeKey } from './common';

export interface TronFreezeBalanceContract extends FreezeBalanceContract {
  resource?: TronResourceCodeKey;
}

export interface TronUnfreezeBalanceContract extends UnfreezeBalanceContract {
  resource?: TronResourceCodeKey;
}

export interface TronWithdrawBalanceContract extends WithdrawBalanceContract {}

export interface TronTransferContract extends TransferContract {}

export interface TronTransactionBalanceTrace extends TransactionBalanceTrace {
  operation?: TronTransactionBalanceTraceOperation[];
}

export interface TronTransactionBalanceTraceOperation extends TransactionBalanceTrace_Operation {}

export interface TronBlockBalanceTrace extends BlockBalanceTrace {
  block_identifier?: TronBlockBalanceTrace_BlockIdentifier;
  transaction_balance_trace?: TronTransactionBalanceTrace[];
}

export interface TronBlockBalanceTrace_BlockIdentifier extends BlockBalanceTrace_BlockIdentifier {}

export interface TronAccountTrace extends AccountTrace {}

export interface TronAccountIdentifier extends AccountIdentifier {}

export interface TronAccountBalanceRequest extends AccountBalanceRequest {
  account_identifier?: TronAccountIdentifier;
  block_identifier?: TronBlockBalanceTrace_BlockIdentifier;
}

export interface TronAccountBalanceResponse extends AccountBalanceResponse {
  block_identifier?: TronBlockBalanceTrace_BlockIdentifier;
}

export interface TronFreezeBalanceV2Contract extends FreezeBalanceV2Contract {
  resource?: TronResourceCodeKey;
}

export interface TronUnfreezeBalanceV2Contract extends UnfreezeBalanceV2Contract {
  resource?: TronResourceCodeKey;
}

export interface TronWithdrawExpireUnfreezeContract extends WithdrawExpireUnfreezeContract {}

export interface TronDelegateResourceContract extends DelegateResourceContract {
  resource?: TronResourceCodeKey;
}

export interface TronUnDelegateResourceContract extends UnDelegateResourceContract {
  resource?: TronResourceCodeKey;
}
