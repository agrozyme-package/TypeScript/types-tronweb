import { ResourceCode } from '../../../protocol/core/contract/common';

export type TronResourceCode = ResourceCode;
export type TronResourceCodeKey = keyof typeof ResourceCode;
