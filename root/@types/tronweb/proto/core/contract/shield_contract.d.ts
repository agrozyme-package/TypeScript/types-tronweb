import {
  AuthenticationPath,
  IncrementalMerkleTree,
  IncrementalMerkleVoucher,
  IncrementalMerkleVoucherInfo,
  MerklePath,
  OutputPoint,
  OutputPointInfo,
  PedersenHash,
  ReceiveDescription,
  ShieldedTransferContract,
  SpendDescription,
} from '../../../protocol/core/contract/shield_contract';

export interface TronAuthenticationPath extends AuthenticationPath {}

export interface TronMerklePath extends MerklePath {
  authentication_paths?: TronAuthenticationPath[];
}

export interface TronOutputPoint extends OutputPoint {}

export interface TronOutputPointInfo extends OutputPointInfo {
  out_points?: TronOutputPoint[];
}

export interface TronPedersenHash extends PedersenHash {}

export interface TronIncrementalMerkleTree extends IncrementalMerkleTree {
  left?: TronPedersenHash;
  right?: TronPedersenHash;
  parents?: TronPedersenHash[];
}

export interface TronIncrementalMerkleVoucher extends IncrementalMerkleVoucher {
  tree?: TronIncrementalMerkleTree;
  filled?: TronPedersenHash[];
  cursor?: TronIncrementalMerkleTree;
  output_point?: TronOutputPoint;
}

export interface TronIncrementalMerkleVoucherInfo extends IncrementalMerkleVoucherInfo {
  vouchers?: TronIncrementalMerkleVoucher[];
}

export interface TronSpendDescription extends SpendDescription {}

export interface TronReceiveDescription extends ReceiveDescription {}

export interface TronShieldedTransferContract extends ShieldedTransferContract {
  spend_description?: TronSpendDescription[];
  receive_description?: TronReceiveDescription[];
}
