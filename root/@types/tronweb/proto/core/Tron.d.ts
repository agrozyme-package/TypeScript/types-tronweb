import {
  Account,
  Account_AccountResource,
  Account_AssetEntry,
  Account_AssetV2Entry,
  Account_FreeAssetNetUsageEntry,
  Account_FreeAssetNetUsageV2Entry,
  Account_FreezeV2,
  Account_Frozen,
  Account_LatestAssetOperationTimeEntry,
  Account_LatestAssetOperationTimeV2Entry,
  Account_UnFreezeV2,
  AccountId,
  AccountType,
  authority,
  Block,
  BlockHeader,
  BlockHeader_raw,
  BlockInventory,
  BlockInventory_BlockId,
  BlockInventory_Type,
  ChainInventory,
  ChainInventory_BlockId,
  ChainParameters,
  ChainParameters_ChainParameter,
  DelegatedResource,
  DelegatedResourceAccountIndex,
  DisconnectMessage,
  DynamicProperties,
  Exchange,
  HelloMessage,
  HelloMessage_BlockId,
  InternalTransaction,
  InternalTransaction_CallValueInfo,
  Inventory,
  Inventory_InventoryType,
  Items,
  Items_ItemType,
  Key,
  MarketAccountOrder,
  MarketOrder,
  MarketOrder_State,
  MarketOrderDetail,
  MarketOrderIdList,
  MarketOrderList,
  MarketOrderPair,
  MarketOrderPairList,
  MarketPrice,
  MarketPriceList,
  MetricsInfo,
  MetricsInfo_BlockChainInfo,
  MetricsInfo_BlockChainInfo_DupWitness,
  MetricsInfo_BlockChainInfo_Witness,
  MetricsInfo_NetInfo,
  MetricsInfo_NetInfo_ApiInfo,
  MetricsInfo_NetInfo_ApiInfo_ApiDetailInfo,
  MetricsInfo_NetInfo_DisconnectionDetailInfo,
  MetricsInfo_NetInfo_LatencyInfo,
  MetricsInfo_NetInfo_LatencyInfo_LatencyDetailInfo,
  MetricsInfo_NodeInfo,
  MetricsInfo_RateInfo,
  NodeInfo,
  NodeInfo_CheatWitnessInfoMapEntry,
  NodeInfo_ConfigNodeInfo,
  NodeInfo_MachineInfo,
  NodeInfo_MachineInfo_DeadLockThreadInfo,
  NodeInfo_MachineInfo_MemoryDescInfo,
  NodeInfo_PeerInfo,
  PBFTCommitResult,
  PBFTMessage,
  PBFTMessage_DataType,
  PBFTMessage_MsgType,
  PBFTMessage_Raw,
  Permission,
  Permission_PermissionType,
  Proposal,
  Proposal_ParametersEntry,
  Proposal_State,
  ReasonCode,
  ResourceReceipt,
  SRL,
  Transaction,
  Transaction_Contract,
  Transaction_Contract_ContractType,
  Transaction_raw,
  Transaction_Result,
  Transaction_Result_code,
  Transaction_Result_contractResult,
  TransactionInfo,
  TransactionInfo_code,
  TransactionInfo_Log,
  TransactionRet,
  Transactions,
  TXInput,
  TXInput_raw,
  TXOutput,
  TXOutputs,
  Vote,
  Votes,
  Witness,
} from '../../protocol/core/Tron';

import { RequiredPick } from '../../utility';
import { TronAny } from '../google';
import { TronResourceCodeKey } from './contract';
import { TronEndpoint } from './Discover';

export type TronAccountType = AccountType;
export type TronAccountTypeKey = keyof typeof AccountType;

export type TronReasonCode = ReasonCode;
export type TronReasonCodeKey = keyof typeof ReasonCode;

export interface TronAccountId extends AccountId {}

interface VoteInterface extends Vote {}
export interface TronVote extends Required<VoteInterface> {}

interface ProposalInterface extends Proposal {
  parameters?: TronProposalParametersEntry[];
  state?: TronProposalStateKey;
}
export interface TronProposal
  extends RequiredPick<
    ProposalInterface,
    'proposal_id' | 'proposer_address' | 'parameters' | 'expiration_time' | 'create_time' | 'state'
  > {}

export type TronProposalState = Proposal_State;
export type TronProposalStateKey = keyof typeof Proposal_State;

export interface TronProposalParametersEntry extends Proposal_ParametersEntry {}

interface ExchangeInterface extends Exchange {}
export interface TronExchange extends Required<ExchangeInterface> {}

export interface TronMarketOrder extends MarketOrder {
  state?: TronMarketOrderStateKey;
}

export type TronMarketOrderState = MarketOrder_State;
export type TronMarketOrderStateKey = keyof typeof MarketOrder_State;

export interface TronMarketOrderList extends MarketOrderList {
  orders?: TronMarketOrder[];
}

export interface TronMarketOrderPairList extends MarketOrderPairList {
  orderPair?: TronMarketOrderPair[];
}

export interface TronMarketOrderPair extends MarketOrderPair {}

export interface TronMarketAccountOrder extends MarketAccountOrder {}

export interface TronMarketPrice extends MarketPrice {}

export interface TronMarketPriceList extends MarketPriceList {
  prices?: TronMarketPrice[];
}

export interface TronMarketOrderIdList extends MarketOrderIdList {}

export interface TronChainParameters extends ChainParameters {
  chainParameter?: TronChainParametersChainParameter[];
}

interface ChainParametersChainParameterInterface extends ChainParameters_ChainParameter {}
export interface TronChainParametersChainParameter extends Required<ChainParametersChainParameterInterface> {}

interface AccountInterface extends Account {}
export interface TronAccount
  extends RequiredPick<
    AccountInterface,
    'address' | 'balance' | 'assetV2' | 'free_asset_net_usageV2' | 'net_window_size'
  > {
  type?: TronAccountTypeKey;
  votes?: TronVote[];
  frozen?: TronAccountFrozen[];
  tron_power?: TronAccountFrozen;
  frozen_supply?: TronAccountFrozen[];
  account_resource: TronAccountAccountResource;
  owner_permission?: TronPermission;
  witness_permission?: TronPermission;
  active_permission?: TronPermission[];
  frozenV2: TronAccountFreezeV2[];
  unfrozenV2?: TronAccountUnFreezeV2[];
}

export interface TronAccountFrozen extends Account_Frozen {}

export interface TronAccountAssetEntry extends Account_AssetEntry {}

export interface TronAccountAssetV2Entry extends Account_AssetV2Entry {}

export interface TronAccountLatestAssetOperationTimeEntry extends Account_LatestAssetOperationTimeEntry {}

export interface TronAccountLatestAssetOperationTimeV2Entry extends Account_LatestAssetOperationTimeV2Entry {}

export interface TronAccountFreeAssetNetUsageEntry extends Account_FreeAssetNetUsageEntry {}

export interface TronAccountFreeAssetNetUsageV2Entry extends Account_FreeAssetNetUsageV2Entry {}

interface AccountAccountResourceInterface extends Account_AccountResource {}
export interface TronAccountAccountResource extends RequiredPick<Account_AccountResource, 'energy_window_size'> {
  frozen_balance_for_energy?: TronAccountFrozen;
}

export interface TronAccountFreezeV2 extends Account_FreezeV2 {
  type?: TronResourceCodeKey;
}

export interface TronAccountUnFreezeV2 extends Account_UnFreezeV2 {
  type?: TronResourceCodeKey;
}

interface KeyInterface extends Key {}
export interface TronKey extends Required<KeyInterface> {}

interface DelegatedResourceInterface extends DelegatedResource {}
export interface TronDelegatedResource extends RequiredPick<DelegatedResourceInterface, 'from' | 'to'> {}

export interface TronAuthority extends authority {
  account?: TronAccountId;
}

interface PermissionInterface extends Permission {}
export interface TronPermission extends RequiredPick<PermissionInterface, 'permission_name' | 'threshold'> {
  type?: TronPermissionPermissionTypeKey;
  keys: TronKey[];
}

export type TronPermissionPermissionType = Permission_PermissionType;
export type TronPermissionPermissionTypeKey = keyof typeof Permission_PermissionType;

interface WitnessInterface extends Witness {}
export interface TronWitness extends RequiredPick<WitnessInterface, 'address' | 'url'> {}

export interface TronVotes extends Votes {
  old_votes?: TronVote[];
  new_votes?: TronVote[];
}

export interface TronTransactionOutput extends TXOutput {}

export interface TronTransactionInput extends TXInput {
  raw_data?: TronTransactionInputRawData;
}

export interface TronTransactionInputRawData extends TXInput_raw {}

export interface TronTransactionOutputs extends TXOutputs {
  outputs?: TronTransactionOutput[];
}

export interface TronResourceReceipt extends ResourceReceipt {
  result?: TronTransactionResultContractResultKey;
}

export interface TronMarketOrderDetail extends MarketOrderDetail {}

export interface TronTransaction extends Transaction {
  txID: string;
  raw_data_hex: string;
  raw_data: TronTransactionRawData;
  ret?: TronTransactionResult[];
}

export interface TronTransactionContract extends Transaction_Contract {
  type: TronTransactionContractContractTypeKey;
  parameter: TronAny;
}

export type TronTransactionContractContractType = Transaction_Contract_ContractType;
export type TronTransactionContractContractTypeKey = keyof typeof Transaction_Contract_ContractType;

export interface TronTransactionResult extends Transaction_Result {
  ret?: TronTransactionResultCodeKey;
  contractRet: TronTransactionResultContractResultKey;
  orderDetails?: TronMarketOrderDetail[];
}

export type TronTransactionResultCode = Transaction_Result_code;
export type TronTransactionResultCodeKey = keyof typeof Transaction_Result_code;

export type TronTransactionResultContractResult = Transaction_Result_contractResult;
export type TronTransactionResultContractResultKey = keyof typeof Transaction_Result_contractResult;

export interface TronTransactionRawData extends Transaction_raw {
  auths?: TronAuthority[];
  contract: TronTransactionContract[];
}

interface TransactionInfoInterface extends TransactionInfo {}
export interface TronTransactionInfo
  extends RequiredPick<TransactionInfoInterface, 'id' | 'fee' | 'blockNumber' | 'blockTimeStamp' | 'contractResult'> {
  receipt?: TronResourceReceipt;
  log?: TronTransactionInfoLog[];
  result?: TronTransactionInfoCodeKey;
  internal_transactions?: TronInternalTransaction[];
  orderDetails?: TronMarketOrderDetail[];
}

export type TronTransactionInfoCode = TransactionInfo_code;
export type TronTransactionInfoCodeKey = keyof typeof TransactionInfo_code;

interface TransactionInfoLogInterface extends TransactionInfo_Log {}
export interface TronTransactionInfoLog extends Required<TransactionInfoLogInterface> {}

export interface TronTransactionRet extends TransactionRet {
  transactioninfo?: TronTransactionInfo[];
}

export interface TronTransactions extends Transactions {
  transactions?: TronTransaction[];
}

interface BlockHeaderInterface extends BlockHeader {}
export interface TronBlockHeader extends RequiredPick<BlockHeaderInterface, 'witness_signature'> {
  raw_data: TronBlockHeaderRawData;
}

interface BlockHeaderRawDataInterface extends BlockHeader_raw {}
export interface TronBlockHeaderRawData
  extends RequiredPick<BlockHeaderRawDataInterface, 'txTrieRoot', 'parentHash', 'witness_address'> {}

export interface TronBlock extends Block {
  blockID: string;
  transactions?: TronTransaction[];
  block_header: TronBlockHeader;
}

export interface TronChainInventory extends ChainInventory {
  ids?: TronChainInventoryBlockId[];
}

export interface TronChainInventoryBlockId extends ChainInventory_BlockId {}

export interface TronBlockInventory extends BlockInventory {
  ids?: TronBlockInventoryBlockId[];
  type?: TronBlockInventoryTypeKey;
}

export type TronBlockInventoryType = BlockInventory_Type;
export type TronBlockInventoryTypeKey = keyof typeof BlockInventory_Type;

export interface TronInventory extends Inventory {
  type?: TronInventoryInventoryTypeKey;
}

export type TronInventoryInventoryType = Inventory_InventoryType;
export type TronInventoryInventoryTypeKey = keyof typeof Inventory_InventoryType;

export interface TronItems extends Items {
  type?: TronItemsItemTypeKey;
  blocks?: TronBlock[];
  block_headers?: TronBlockHeader[];
  transactions?: TronTransaction[];
}

export type TronItemsItemType = Items_ItemType;
export type TronItemsItemTypeKey = keyof typeof Items_ItemType;

export interface TronDynamicProperties extends DynamicProperties {}

export interface TronDisconnectMessage extends DisconnectMessage {
  reason?: TronReasonCodeKey;
}

export interface TronHelloMessage extends HelloMessage {
  from?: TronEndpoint;
  genesisBlockId?: TronHelloMessageBlockId;
  solidBlockId?: TronHelloMessageBlockId;
  headBlockId?: TronHelloMessageBlockId;
}

export interface TronHelloMessageBlockId extends HelloMessage_BlockId {}

export interface TronBlockInventoryBlockId extends BlockInventory_BlockId {}

interface InternalTransactionInterface extends InternalTransaction {}
export interface TronInternalTransaction
  extends RequiredPick<InternalTransactionInterface, 'hash' | 'caller_address' | 'transferTo_address' | 'note'> {
  callValueInfo: TronInternalTransaction_CallValueInfo[];
}

export interface TronInternalTransaction_CallValueInfo extends InternalTransaction_CallValueInfo {}

interface DelegatedResourceAccountIndexInterface extends DelegatedResourceAccountIndex {}
export interface TronDelegatedResourceAccountIndex
  extends RequiredPick<DelegatedResourceAccountIndexInterface, 'account'> {}

interface NodeInfoInterface extends NodeInfo {
  peerInfoList?: TronNodeInfoPeerInfo[];
  configNodeInfo?: TronNodeInfoConfigNodeInfo;
  machineInfo?: TronNodeInfoMachineInfo;
}
export interface TronNodeInfo extends Required<Omit<NodeInfoInterface, 'peerInfoList'>> {
  peerList: TronNodeInfoPeerInfo[];
}

export interface TronNodeInfoCheatWitnessInfoMapEntry extends NodeInfo_CheatWitnessInfoMapEntry {}

interface NodeInfoPeerInfoInterface extends NodeInfo_PeerInfo {}
export interface TronNodeInfoPeerInfo extends Required<Omit<NodeInfoPeerInfoInterface, 'isActive'>> {
  active: boolean;
}

interface NodeInfoConfigNodeInfoInterface extends NodeInfo_ConfigNodeInfo {}
export interface TronNodeInfoConfigNodeInfo extends Required<NodeInfoConfigNodeInfoInterface> {}

interface NodeInfoMachineInfoInterface extends NodeInfo_MachineInfo {
  memoryDescInfoList?: TronNodeInfoMachineInfoMemoryDescInfo[];
  deadLockThreadInfoList?: TronNodeInfoMachineInfoDeadLockThreadInfo[];
}
export interface TronNodeInfoMachineInfo extends Required<NodeInfoMachineInfoInterface> {}

interface NodeInfoMachineInfoMemoryDescInfoInterface extends NodeInfo_MachineInfo_MemoryDescInfo {}
export interface TronNodeInfoMachineInfoMemoryDescInfo extends Required<NodeInfoMachineInfoMemoryDescInfoInterface> {}

interface NodeInfoMachineInfoDeadLockThreadInfoInterface extends NodeInfo_MachineInfo_DeadLockThreadInfo {}
export interface TronNodeInfoMachineInfoDeadLockThreadInfo
  extends Required<NodeInfoMachineInfoDeadLockThreadInfoInterface> {}

export interface TronMetricsInfo extends MetricsInfo {
  node?: TronMetricsInfoNodeInfo;
  blockchain?: TronMetricsInfoBlockChainInfo;
  net?: TronMetricsInfoNetInfo;
}

export interface TronMetricsInfoNodeInfo extends MetricsInfo_NodeInfo {}

export interface TronMetricsInfoBlockChainInfo extends MetricsInfo_BlockChainInfo {
  blockProcessTime?: TronMetricsInfoRateInfo;
  tps?: TronMetricsInfoRateInfo;
  missedTransaction?: TronMetricsInfoRateInfo;
  witnesses?: TronMetricsInfoBlockChainInfoWitness[];
  dupWitness?: TronMetricsInfoBlockChainInfoDupWitness[];
}

export interface TronMetricsInfoBlockChainInfoWitness extends MetricsInfo_BlockChainInfo_Witness {}

export interface TronMetricsInfoBlockChainInfoDupWitness extends MetricsInfo_BlockChainInfo_DupWitness {}

export interface TronMetricsInfoRateInfo extends MetricsInfo_RateInfo {}

export interface TronMetricsInfoNetInfo extends MetricsInfo_NetInfo {
  api?: TronMetricsInfoNetInfoApiInfo;
  tcpInTraffic?: TronMetricsInfoRateInfo;
  tcpOutTraffic?: TronMetricsInfoRateInfo;
  disconnectionDetail?: TronMetricsInfoNetInfoDisconnectionDetailInfo[];
  udpInTraffic?: TronMetricsInfoRateInfo;
  udpOutTraffic?: TronMetricsInfoRateInfo;
  latency?: TronMetricsInfoNetInfoLatencyInfo;
}

export interface TronMetricsInfoNetInfoApiInfo extends MetricsInfo_NetInfo_ApiInfo {
  qps?: TronMetricsInfoRateInfo;
  failQps?: TronMetricsInfoRateInfo;
  outTraffic?: TronMetricsInfoRateInfo;
  detail?: TronMetricsInfoNetInfoApiInfoApiDetailInfo[];
}

export interface TronMetricsInfoNetInfoApiInfoApiDetailInfo extends MetricsInfo_NetInfo_ApiInfo_ApiDetailInfo {
  qps?: TronMetricsInfoRateInfo;
  failQps?: TronMetricsInfoRateInfo;
  outTraffic?: TronMetricsInfoRateInfo;
}

export interface TronMetricsInfoNetInfoDisconnectionDetailInfo extends MetricsInfo_NetInfo_DisconnectionDetailInfo {}

export interface TronMetricsInfoNetInfoLatencyInfo extends MetricsInfo_NetInfo_LatencyInfo {
  detail?: TronMetricsInfoNetInfoLatencyInfoLatencyDetailInfo[];
}

export interface TronMetricsInfoNetInfoLatencyInfoLatencyDetailInfo
  extends MetricsInfo_NetInfo_LatencyInfo_LatencyDetailInfo {}

export interface TronPbftMessage extends PBFTMessage {
  raw_data?: TronPbftMessageRawData;
}

export type TronPbftMessageMessageType = PBFTMessage_MsgType;
export type TronPbftMessageMessageTypeKey = keyof typeof PBFTMessage_MsgType;

export type TronPbftMessageDataType = PBFTMessage_DataType;
export type TronPbftMessageDataTypeKey = keyof typeof PBFTMessage_DataType;

export interface TronPbftMessageRawData extends PBFTMessage_Raw {
  msg_type?: TronPbftMessageMessageTypeKey;
  data_type?: TronPbftMessageDataTypeKey;
}

export interface TronPbftCommitResult extends PBFTCommitResult {}

export interface TronSRL extends SRL {}
