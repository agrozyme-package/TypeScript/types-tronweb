import {
  BackupMessage,
  Endpoint,
  FindNeighbours,
  Neighbours,
  PingMessage,
  PongMessage,
} from '../../protocol/core/Discover';

export interface TronEndpoint extends Endpoint {}

export interface TronPingMessage extends PingMessage {
  from?: TronEndpoint;
  to?: TronEndpoint;
}

export interface TronPongMessage extends PongMessage {
  from?: TronEndpoint;
}

export interface TronFindNeighbours extends FindNeighbours {
  from?: TronEndpoint;
}

export interface TronNeighbours extends Neighbours {
  from?: TronEndpoint;
  neighbours?: TronEndpoint[];
}

export interface TronBackupMessage extends BackupMessage {}
