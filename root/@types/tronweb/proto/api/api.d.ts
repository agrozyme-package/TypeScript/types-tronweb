import {
  AccountNetMessage,
  AccountNetMessage_AssetNetLimitEntry,
  AccountNetMessage_AssetNetUsedEntry,
  AccountPaginated,
  AccountResourceMessage,
  AccountResourceMessage_AssetNetLimitEntry,
  AccountResourceMessage_AssetNetUsedEntry,
  Address,
  AssetIssueList,
  BlockExtention,
  BlockIncrementalMerkleTree,
  BlockLimit,
  BlockList,
  BlockListExtention,
  BlockReference,
  BlockReq,
  BytesMessage,
  CanDelegatedMaxSizeRequestMessage,
  CanDelegatedMaxSizeResponseMessage,
  CanWithdrawUnfreezeAmountRequestMessage,
  CanWithdrawUnfreezeAmountResponseMessage,
  DecryptNotes,
  DecryptNotes_NoteTx,
  DecryptNotesMarked,
  DecryptNotesMarked_NoteTx,
  DecryptNotesTRC20,
  DecryptNotesTRC20_NoteTx,
  DelegatedResourceList,
  DelegatedResourceMessage,
  DiversifierMessage,
  EmptyMessage,
  EstimateEnergyMessage,
  ExchangeList,
  ExpandedSpendingKeyMessage,
  GetAvailableUnfreezeCountRequestMessage,
  GetAvailableUnfreezeCountResponseMessage,
  IncomingViewingKeyDiversifierMessage,
  IncomingViewingKeyMessage,
  IvkDecryptAndMarkParameters,
  IvkDecryptParameters,
  IvkDecryptTRC20Parameters,
  NfParameters,
  NfTRC20Parameters,
  Node,
  NodeList,
  Note,
  NoteParameters,
  NullifierResult,
  NumberMessage,
  OvkDecryptParameters,
  OvkDecryptTRC20Parameters,
  PaginatedMessage,
  PaymentAddressMessage,
  PrivateParameters,
  PrivateParametersWithoutAsk,
  PrivateShieldedTRC20Parameters,
  PrivateShieldedTRC20ParametersWithoutAsk,
  ProposalList,
  ReceiveNote,
  Return,
  Return_response_code,
  ShieldedAddressInfo,
  ShieldedTRC20Parameters,
  ShieldedTRC20TriggerContractParameters,
  SpendAuthSigParameters,
  SpendNote,
  SpendNoteTRC20,
  SpendResult,
  TimeMessage,
  TimePaginatedMessage,
  TransactionApprovedList,
  TransactionApprovedList_Result,
  TransactionApprovedList_Result_response_code,
  TransactionExtention,
  TransactionIdList,
  TransactionInfoList,
  TransactionLimit,
  TransactionList,
  TransactionListExtention,
  TransactionSignWeight,
  TransactionSignWeight_Result,
  TransactionSignWeight_Result_response_code,
  ViewingKeyMessage,
  WitnessList,
} from '../../protocol/api/api';

import { RequiredPick } from '../../utility';
import {
  TronAccount,
  TronAssetIssueContract,
  TronBlock,
  TronBlockHeader,
  TronDelegatedResource,
  TronExchange,
  TronIncrementalMerkleTree,
  TronIncrementalMerkleVoucher,
  TronInternalTransaction,
  TronPermission,
  TronProposal,
  TronReceiveDescription,
  TronSpendDescription,
  TronTransaction,
  TronTransactionInfo,
  TronTransactionInfoLog,
  TronWitness,
} from '../core';

export interface TronReturn extends Return {
  code?: TronReturnResponseCodeKey;
}

export type TronReturnResponseCode = Return_response_code;
export type TronReturnResponseCodeKey = keyof typeof Return_response_code;

export interface TronBlockReference extends BlockReference {}

export interface TronWitnessList extends WitnessList {
  witnesses?: TronWitness[];
}

export interface TronProposalList extends ProposalList {
  proposals?: TronProposal[];
}

export interface TronExchangeList extends ExchangeList {
  exchanges?: TronExchange[];
}

export interface TronAssetIssueList extends AssetIssueList {
  assetIssue?: TronAssetIssueContract[];
}

export interface TronBlockList extends BlockList {
  block?: TronBlock[];
}

export interface TronTransactionList extends TransactionList {
  transaction?: TronTransaction[];
}

export interface TronTransactionIdList extends TransactionIdList {}

export interface TronDelegatedResourceMessage extends DelegatedResourceMessage {}

export interface TronDelegatedResourceList extends DelegatedResourceList {
  delegatedResource?: TronDelegatedResource[];
}

export interface TronGetAvailableUnfreezeCountRequestMessage extends GetAvailableUnfreezeCountRequestMessage {}

export interface TronGetAvailableUnfreezeCountResponseMessage extends GetAvailableUnfreezeCountResponseMessage {}

export interface TronCanDelegatedMaxSizeRequestMessage extends CanDelegatedMaxSizeRequestMessage {}

export interface TronCanDelegatedMaxSizeResponseMessage extends CanDelegatedMaxSizeResponseMessage {}

export interface TronCanWithdrawUnfreezeAmountRequestMessage extends CanWithdrawUnfreezeAmountRequestMessage {}

export interface TronCanWithdrawUnfreezeAmountResponseMessage extends CanWithdrawUnfreezeAmountResponseMessage {}

export interface TronNodeList extends NodeList {
  nodes?: TronNode[];
}

export interface TronNode extends Node {
  address?: Address;
}

export interface TronAddress extends Address {}

export interface TronEmptyMessage extends EmptyMessage {}

export interface TronNumberMessage extends NumberMessage {}

export interface TronBytesMessage extends BytesMessage {}

export interface TronTimeMessage extends TimeMessage {}

export interface TronBlockRequest extends BlockReq {}

export interface TronBlockLimit extends BlockLimit {}

export interface TronTransactionLimit extends TransactionLimit {}

export interface TronAccountPaginated extends AccountPaginated {
  account?: TronAccount;
}

export interface TronTimePaginatedMessage extends TimePaginatedMessage {
  timeMessage?: TronTimeMessage;
}

export interface TronAccountNetMessage extends AccountNetMessage {}

export interface TronAccountNetMessageAssetNetUsedEntry extends AccountNetMessage_AssetNetUsedEntry {}

export interface TronAccountNetMessageAssetNetLimitEntry extends AccountNetMessage_AssetNetLimitEntry {}

interface AccountResourceMessageInterface extends AccountResourceMessage {
  assetNetUsed?: TronAccountResourceMessageAssetNetUsedEntry[];
  assetNetLimit?: TronAccountResourceMessageAssetNetLimitEntry[];
}
export interface TronAccountResourceMessage
  extends RequiredPick<
    AccountResourceMessageInterface,
    | 'freeNetLimit'
    | 'assetNetUsed'
    | 'assetNetLimit'
    | 'TotalNetLimit'
    | 'TotalNetWeight'
    | 'TotalEnergyLimit'
    | 'TotalEnergyWeight'
  > {}

export interface TronAccountResourceMessageAssetNetUsedEntry extends AccountResourceMessage_AssetNetUsedEntry {}

export interface TronAccountResourceMessageAssetNetLimitEntry extends AccountResourceMessage_AssetNetLimitEntry {}

export interface TronPaginatedMessage extends PaginatedMessage {}

interface TransactionExtentionInterface extends TransactionExtention {
  transaction?: TronTransaction;
  result?: TronReturn;
  logs?: TronTransactionInfoLog[];
  internal_transactions?: TronInternalTransaction[];
}
export interface TronTransactionExtention
  extends RequiredPick<TransactionExtentionInterface, 'transaction' | 'result'> {}

export interface TronEstimateEnergyMessage extends Required<EstimateEnergyMessage> {
  result?: TronReturn;
}

export interface TronBlockExtention extends BlockExtention {
  transactions?: TronTransactionExtention[];
  block_header?: TronBlockHeader;
}

export interface TronBlockListExtention extends BlockListExtention {
  block?: TronBlockExtention[];
}

export interface TronTransactionListExtention extends TransactionListExtention {
  transaction?: TronTransactionExtention[];
}

export interface TronBlockIncrementalMerkleTree extends BlockIncrementalMerkleTree {
  merkleTree?: TronIncrementalMerkleTree;
}

interface TransactionSignWeightInterface extends TransactionSignWeight {
  permission?: TronPermission;
  result?: TronTransactionSignWeightResult;
  transaction?: TronTransactionExtention;
}
export interface TronTransactionSignWeight extends Required<TransactionSignWeightInterface> {}

export interface TronTransactionSignWeightResult extends TransactionSignWeight_Result {
  code?: TronTransactionSignWeightResultResponseCodeKey;
}

export type TronTransactionSignWeightResultResponseCode = TransactionSignWeight_Result_response_code;
export type TronTransactionSignWeightResultResponseCodeKey = keyof typeof TransactionSignWeight_Result_response_code;

interface TransactionApprovedListInterface extends TransactionApprovedList {
  result?: TronTransactionApprovedListResult;
  transaction?: TransactionExtention;
}
export interface TronTransactionApprovedList extends Required<TransactionApprovedListInterface> {}

export interface TronTransactionApprovedListResult extends TransactionApprovedList_Result {
  code?: TronTransactionApprovedListResultResponseCodeKey;
}

export type TronTransactionApprovedListResultResponseCode = TransactionApprovedList_Result_response_code;
export type TronTransactionApprovedListResultResponseCodeKey =
  keyof typeof TransactionApprovedList_Result_response_code;

export interface TronIvkDecryptParameters extends IvkDecryptParameters {}

export interface TronIvkDecryptAndMarkParameters extends IvkDecryptAndMarkParameters {}

export interface TronOvkDecryptParameters extends OvkDecryptParameters {}

export interface TronDecryptNotes extends DecryptNotes {
  noteTxs?: TronDecryptNotesNoteTransaction[];
}

export interface TronDecryptNotesNoteTransaction extends DecryptNotes_NoteTx {
  note?: TronNote;
}

export interface TronDecryptNotesMarked extends DecryptNotesMarked {
  noteTxs?: TronDecryptNotesMarkedNoteTransaction[];
}

export interface TronDecryptNotesMarkedNoteTransaction extends DecryptNotesMarked_NoteTx {
  note?: TronNote;
}

export interface TronNote extends Note {}

export interface TronSpendNote extends SpendNote {
  note?: TronNote;
  voucher?: TronIncrementalMerkleVoucher;
}

export interface TronReceiveNote extends ReceiveNote {
  note?: TronNote;
}

export interface TronPrivateParameters extends PrivateParameters {
  shielded_spends?: TronSpendNote[];
  shielded_receives?: TronReceiveNote[];
}

export interface TronPrivateParametersWithoutAsk extends PrivateParametersWithoutAsk {
  shielded_spends?: TronSpendNote[];
  shielded_receives?: TronReceiveNote[];
}

export interface TronSpendAuthSigParameters extends SpendAuthSigParameters {}

export interface TronNfParameters extends NfParameters {
  note?: TronNote;
  voucher?: TronIncrementalMerkleVoucher;
}

export interface TronExpandedSpendingKeyMessage extends ExpandedSpendingKeyMessage {}

export interface TronViewingKeyMessage extends ViewingKeyMessage {}

export interface TronIncomingViewingKeyMessage extends IncomingViewingKeyMessage {}

export interface TronDiversifierMessage extends DiversifierMessage {}

export interface TronIncomingViewingKeyDiversifierMessage extends IncomingViewingKeyDiversifierMessage {
  ivk?: TronIncomingViewingKeyMessage;
  d?: TronDiversifierMessage;
}

export interface TronPaymentAddressMessage extends PaymentAddressMessage {
  d?: TronDiversifierMessage;
}

export interface TronShieldedAddressInfo extends ShieldedAddressInfo {}

export interface TronNoteParameters extends NoteParameters {
  note?: TronNote;
}

export interface TronSpendResult extends SpendResult {}

export interface TronTransactionInfoList extends TransactionInfoList {
  transactionInfo?: TronTransactionInfo[];
}

export interface TronSpendNoteTrc20 extends SpendNoteTRC20 {
  note?: TronNote;
}

export interface TronPrivateShieldedTrc20Parameters extends PrivateShieldedTRC20Parameters {
  shielded_spends?: TronSpendNoteTrc20[];
  shielded_receives?: TronReceiveNote[];
}

export interface TronPrivateShieldedTrc20ParametersWithoutAsk extends PrivateShieldedTRC20ParametersWithoutAsk {
  shielded_spends?: TronSpendNoteTrc20[];
  shielded_receives?: TronReceiveNote[];
}

export interface TronShieldedTrc20Parameters extends ShieldedTRC20Parameters {
  spend_description?: TronSpendDescription[];
  receive_description?: TronReceiveDescription[];
}

export interface TronIvkDecryptTrc20Parameters extends IvkDecryptTRC20Parameters {}

export interface TronOvkDecryptTrc20Parameters extends OvkDecryptTRC20Parameters {}

export interface TronDecryptNotesTrc20 extends DecryptNotesTRC20 {
  noteTxs?: TronDecryptNotesTrc20NoteTransaction[];
}

export interface TronDecryptNotesTrc20NoteTransaction extends DecryptNotesTRC20_NoteTx {
  note?: TronNote;
}

export interface TronNfTrc20Parameters extends NfTRC20Parameters {
  note?: TronNote;
}

export interface TronNullifierResult extends NullifierResult {}

export interface TronShieldedTrc20TriggerContractParameters extends ShieldedTRC20TriggerContractParameters {
  shielded_TRC20_Parameters?: TronShieldedTrc20Parameters;
  spend_authority_signature?: TronBytesMessage[];
}
