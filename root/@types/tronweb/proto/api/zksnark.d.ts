import { ZksnarkRequest, ZksnarkResponse, ZksnarkResponse_Code } from '../../protocol/api/zksnark';
import { TronTransaction } from '../core';

export interface TronZksnarkRequest extends ZksnarkRequest {
  transaction?: TronTransaction;
}

export interface TronZksnarkResponse extends ZksnarkResponse {
  code?: ZksnarkResponse_Code;
}

export type TronZksnarkResponseCode = ZksnarkResponse_Code;
export type TronZksnarkResponseCodeKey = keyof typeof TronZksnarkResponseCodeKey;
