/* eslint-disable */
import type {
  Account,
  Block,
  BlockHeader,
  DelegatedResource,
  Exchange,
  InternalTransaction,
  Permission,
  Proposal,
  Transaction,
  TransactionInfo,
  TransactionInfo_Log,
  Witness,
} from "../core/Tron";
import type { AssetIssueContract } from "../core/contract/asset_issue_contract";
import type {
  IncrementalMerkleTree,
  IncrementalMerkleVoucher,
  ReceiveDescription,
  SpendDescription,
} from "../core/contract/shield_contract";

export interface Return {
  result?: boolean | undefined;
  code?: Return_response_code | undefined;
  message?: string | undefined;
}

export declare const enum Return_response_code {
  SUCCESS = 0,
  /** SIGERROR - error in signature */
  SIGERROR = 1,
  CONTRACT_VALIDATE_ERROR = 2,
  CONTRACT_EXE_ERROR = 3,
  BANDWITH_ERROR = 4,
  DUP_TRANSACTION_ERROR = 5,
  TAPOS_ERROR = 6,
  TOO_BIG_TRANSACTION_ERROR = 7,
  TRANSACTION_EXPIRATION_ERROR = 8,
  SERVER_BUSY = 9,
  NO_CONNECTION = 10,
  NOT_ENOUGH_EFFECTIVE_CONNECTION = 11,
  OTHER_ERROR = 20,
}

export interface BlockReference {
  block_num?: number | undefined;
  block_hash?: string | undefined;
}

export interface WitnessList {
  witnesses?: Witness[] | undefined;
}

export interface ProposalList {
  proposals?: Proposal[] | undefined;
}

export interface ExchangeList {
  exchanges?: Exchange[] | undefined;
}

export interface AssetIssueList {
  assetIssue?: AssetIssueContract[] | undefined;
}

export interface BlockList {
  block?: Block[] | undefined;
}

export interface TransactionList {
  transaction?: Transaction[] | undefined;
}

export interface TransactionIdList {
  txId?: string[] | undefined;
}

export interface DelegatedResourceMessage {
  fromAddress?: string | undefined;
  toAddress?: string | undefined;
}

export interface DelegatedResourceList {
  delegatedResource?: DelegatedResource[] | undefined;
}

export interface GetAvailableUnfreezeCountRequestMessage {
  owner_address?: string | undefined;
}

export interface GetAvailableUnfreezeCountResponseMessage {
  count?: number | undefined;
}

export interface CanDelegatedMaxSizeRequestMessage {
  type?: number | undefined;
  owner_address?: string | undefined;
}

export interface CanDelegatedMaxSizeResponseMessage {
  max_size?: number | undefined;
}

export interface CanWithdrawUnfreezeAmountRequestMessage {
  owner_address?: string | undefined;
  timestamp?: number | undefined;
}

export interface CanWithdrawUnfreezeAmountResponseMessage {
  amount?: number | undefined;
}

/** Gossip node list */
export interface NodeList {
  nodes?: Node[] | undefined;
}

/** Gossip node */
export interface Node {
  address?: Address | undefined;
}

/** Gossip node address */
export interface Address {
  host?: string | undefined;
  port?: number | undefined;
}

export interface EmptyMessage {
}

export interface NumberMessage {
  num?: number | undefined;
}

export interface BytesMessage {
  value?: string | undefined;
}

export interface TimeMessage {
  beginInMilliseconds?: number | undefined;
  endInMilliseconds?: number | undefined;
}

export interface BlockReq {
  id_or_num?: string | undefined;
  detail?: boolean | undefined;
}

export interface BlockLimit {
  startNum?: number | undefined;
  endNum?: number | undefined;
}

export interface TransactionLimit {
  transactionId?: string | undefined;
  limitNum?: number | undefined;
}

export interface AccountPaginated {
  account?: Account | undefined;
  offset?: number | undefined;
  limit?: number | undefined;
}

export interface TimePaginatedMessage {
  timeMessage?: TimeMessage | undefined;
  offset?: number | undefined;
  limit?: number | undefined;
}

/** deprecated */
export interface AccountNetMessage {
  freeNetUsed?: number | undefined;
  freeNetLimit?: number | undefined;
  NetUsed?: number | undefined;
  NetLimit?: number | undefined;
  assetNetUsed?: { [key: string]: number } | undefined;
  assetNetLimit?: { [key: string]: number } | undefined;
  TotalNetLimit?: number | undefined;
  TotalNetWeight?: number | undefined;
}

export interface AccountNetMessage_AssetNetUsedEntry {
  key: string;
  value: number;
}

export interface AccountNetMessage_AssetNetLimitEntry {
  key: string;
  value: number;
}

export interface AccountResourceMessage {
  freeNetUsed?: number | undefined;
  freeNetLimit?: number | undefined;
  NetUsed?: number | undefined;
  NetLimit?: number | undefined;
  assetNetUsed?: { [key: string]: number } | undefined;
  assetNetLimit?: { [key: string]: number } | undefined;
  TotalNetLimit?: number | undefined;
  TotalNetWeight?: number | undefined;
  TotalTronPowerWeight?: number | undefined;
  tronPowerUsed?: number | undefined;
  tronPowerLimit?: number | undefined;
  EnergyUsed?: number | undefined;
  EnergyLimit?: number | undefined;
  TotalEnergyLimit?: number | undefined;
  TotalEnergyWeight?: number | undefined;
  storageUsed?: number | undefined;
  storageLimit?: number | undefined;
}

export interface AccountResourceMessage_AssetNetUsedEntry {
  key: string;
  value: number;
}

export interface AccountResourceMessage_AssetNetLimitEntry {
  key: string;
  value: number;
}

export interface PaginatedMessage {
  offset?: number | undefined;
  limit?: number | undefined;
}

export interface TransactionExtention {
  transaction?:
    | Transaction
    | undefined;
  /** transaction id =  sha256(transaction.rowdata) */
  txid?: string | undefined;
  constant_result?: string[] | undefined;
  result?: Return | undefined;
  energy_used?: number | undefined;
  logs?: TransactionInfo_Log[] | undefined;
  internal_transactions?: InternalTransaction[] | undefined;
  energy_penalty?: number | undefined;
}

export interface EstimateEnergyMessage {
  result?: Return | undefined;
  energy_required?: number | undefined;
}

export interface BlockExtention {
  transactions?: TransactionExtention[] | undefined;
  block_header?: BlockHeader | undefined;
  blockid?: string | undefined;
}

export interface BlockListExtention {
  block?: BlockExtention[] | undefined;
}

export interface TransactionListExtention {
  transaction?: TransactionExtention[] | undefined;
}

export interface BlockIncrementalMerkleTree {
  number?: number | undefined;
  merkleTree?: IncrementalMerkleTree | undefined;
}

export interface TransactionSignWeight {
  permission?: Permission | undefined;
  approved_list?: string[] | undefined;
  current_weight?: number | undefined;
  result?: TransactionSignWeight_Result | undefined;
  transaction?: TransactionExtention | undefined;
}

export interface TransactionSignWeight_Result {
  code?: TransactionSignWeight_Result_response_code | undefined;
  message?: string | undefined;
}

export declare const enum TransactionSignWeight_Result_response_code {
  ENOUGH_PERMISSION = 0,
  /** NOT_ENOUGH_PERMISSION - error in */
  NOT_ENOUGH_PERMISSION = 1,
  SIGNATURE_FORMAT_ERROR = 2,
  COMPUTE_ADDRESS_ERROR = 3,
  /** PERMISSION_ERROR - The key is not in permission */
  PERMISSION_ERROR = 4,
  OTHER_ERROR = 20,
}

export interface TransactionApprovedList {
  approved_list?: string[] | undefined;
  result?: TransactionApprovedList_Result | undefined;
  transaction?: TransactionExtention | undefined;
}

export interface TransactionApprovedList_Result {
  code?: TransactionApprovedList_Result_response_code | undefined;
  message?: string | undefined;
}

export declare const enum TransactionApprovedList_Result_response_code {
  SUCCESS = 0,
  SIGNATURE_FORMAT_ERROR = 1,
  COMPUTE_ADDRESS_ERROR = 2,
  OTHER_ERROR = 20,
}

export interface IvkDecryptParameters {
  start_block_index?: number | undefined;
  end_block_index?: number | undefined;
  ivk?: string | undefined;
}

export interface IvkDecryptAndMarkParameters {
  start_block_index?: number | undefined;
  end_block_index?: number | undefined;
  ivk?: string | undefined;
  ak?: string | undefined;
  nk?: string | undefined;
}

export interface OvkDecryptParameters {
  start_block_index?: number | undefined;
  end_block_index?: number | undefined;
  ovk?: string | undefined;
}

export interface DecryptNotes {
  noteTxs?: DecryptNotes_NoteTx[] | undefined;
}

export interface DecryptNotes_NoteTx {
  note?:
    | Note
    | undefined;
  /** transaction id =  sha256(transaction.rowdata) */
  txid?:
    | string
    | undefined;
  /** the index of note in receive */
  index?: number | undefined;
}

export interface DecryptNotesMarked {
  noteTxs?: DecryptNotesMarked_NoteTx[] | undefined;
}

export interface DecryptNotesMarked_NoteTx {
  note?:
    | Note
    | undefined;
  /** transaction id =  sha256(transaction.rowdata) */
  txid?:
    | string
    | undefined;
  /** the index of note in receive */
  index?: number | undefined;
  is_spend?: boolean | undefined;
}

export interface Note {
  value?: number | undefined;
  payment_address?:
    | string
    | undefined;
  /** random 32 */
  rcm?: string | undefined;
  memo?: string | undefined;
}

export interface SpendNote {
  note?:
    | Note
    | undefined;
  /** random number for spend authority signature */
  alpha?: string | undefined;
  voucher?:
    | IncrementalMerkleVoucher
    | undefined;
  /** path for cm from leaf to root in merkle tree */
  path?: string | undefined;
}

export interface ReceiveNote {
  note?: Note | undefined;
}

export interface PrivateParameters {
  transparent_from_address?: string | undefined;
  ask?: string | undefined;
  nsk?: string | undefined;
  ovk?: string | undefined;
  from_amount?: number | undefined;
  shielded_spends?: SpendNote[] | undefined;
  shielded_receives?: ReceiveNote[] | undefined;
  transparent_to_address?: string | undefined;
  to_amount?:
    | number
    | undefined;
  /** timeout in seconds, it works only when it bigger than 0 */
  timeout?: number | undefined;
}

export interface PrivateParametersWithoutAsk {
  transparent_from_address?: string | undefined;
  ak?: string | undefined;
  nsk?: string | undefined;
  ovk?: string | undefined;
  from_amount?: number | undefined;
  shielded_spends?: SpendNote[] | undefined;
  shielded_receives?: ReceiveNote[] | undefined;
  transparent_to_address?: string | undefined;
  to_amount?:
    | number
    | undefined;
  /** timeout in seconds, it works only when it bigger than 0 */
  timeout?: number | undefined;
}

export interface SpendAuthSigParameters {
  ask?: string | undefined;
  tx_hash?: string | undefined;
  alpha?: string | undefined;
}

export interface NfParameters {
  note?: Note | undefined;
  voucher?: IncrementalMerkleVoucher | undefined;
  ak?: string | undefined;
  nk?: string | undefined;
}

export interface ExpandedSpendingKeyMessage {
  ask?: string | undefined;
  nsk?: string | undefined;
  ovk?: string | undefined;
}

export interface ViewingKeyMessage {
  ak?: string | undefined;
  nk?: string | undefined;
}

export interface IncomingViewingKeyMessage {
  ivk?: string | undefined;
}

export interface DiversifierMessage {
  d?: string | undefined;
}

export interface IncomingViewingKeyDiversifierMessage {
  ivk?: IncomingViewingKeyMessage | undefined;
  d?: DiversifierMessage | undefined;
}

export interface PaymentAddressMessage {
  d?: DiversifierMessage | undefined;
  pkD?: string | undefined;
  payment_address?: string | undefined;
}

export interface ShieldedAddressInfo {
  sk?: string | undefined;
  ask?: string | undefined;
  nsk?: string | undefined;
  ovk?: string | undefined;
  ak?: string | undefined;
  nk?: string | undefined;
  ivk?: string | undefined;
  d?: string | undefined;
  pkD?: string | undefined;
  payment_address?: string | undefined;
}

export interface NoteParameters {
  ak?: string | undefined;
  nk?: string | undefined;
  note?: Note | undefined;
  txid?: string | undefined;
  index?: number | undefined;
}

export interface SpendResult {
  result?: boolean | undefined;
  message?: string | undefined;
}

export interface TransactionInfoList {
  transactionInfo?: TransactionInfo[] | undefined;
}

export interface SpendNoteTRC20 {
  note?: Note | undefined;
  alpha?: string | undefined;
  root?: string | undefined;
  path?: string | undefined;
  pos?: number | undefined;
}

export interface PrivateShieldedTRC20Parameters {
  ask?: string | undefined;
  nsk?: string | undefined;
  ovk?: string | undefined;
  from_amount?: string | undefined;
  shielded_spends?: SpendNoteTRC20[] | undefined;
  shielded_receives?: ReceiveNote[] | undefined;
  transparent_to_address?: string | undefined;
  to_amount?: string | undefined;
  shielded_TRC20_contract_address?: string | undefined;
}

export interface PrivateShieldedTRC20ParametersWithoutAsk {
  ak?: string | undefined;
  nsk?: string | undefined;
  ovk?: string | undefined;
  from_amount?: string | undefined;
  shielded_spends?: SpendNoteTRC20[] | undefined;
  shielded_receives?: ReceiveNote[] | undefined;
  transparent_to_address?: string | undefined;
  to_amount?: string | undefined;
  shielded_TRC20_contract_address?: string | undefined;
}

export interface ShieldedTRC20Parameters {
  spend_description?: SpendDescription[] | undefined;
  receive_description?: ReceiveDescription[] | undefined;
  binding_signature?: string | undefined;
  message_hash?: string | undefined;
  trigger_contract_input?: string | undefined;
  parameter_type?: string | undefined;
}

export interface IvkDecryptTRC20Parameters {
  start_block_index?: number | undefined;
  end_block_index?: number | undefined;
  shielded_TRC20_contract_address?: string | undefined;
  ivk?: string | undefined;
  ak?: string | undefined;
  nk?: string | undefined;
  events?: string[] | undefined;
}

export interface OvkDecryptTRC20Parameters {
  start_block_index?: number | undefined;
  end_block_index?: number | undefined;
  ovk?: string | undefined;
  shielded_TRC20_contract_address?: string | undefined;
  events?: string[] | undefined;
}

export interface DecryptNotesTRC20 {
  noteTxs?: DecryptNotesTRC20_NoteTx[] | undefined;
}

export interface DecryptNotesTRC20_NoteTx {
  note?: Note | undefined;
  position?: number | undefined;
  is_spent?: boolean | undefined;
  txid?:
    | string
    | undefined;
  /** the index of note in txid */
  index?: number | undefined;
  to_amount?: string | undefined;
  transparent_to_address?: string | undefined;
}

export interface NfTRC20Parameters {
  note?: Note | undefined;
  ak?: string | undefined;
  nk?: string | undefined;
  position?: number | undefined;
  shielded_TRC20_contract_address?: string | undefined;
}

export interface NullifierResult {
  is_spent?: boolean | undefined;
}

export interface ShieldedTRC20TriggerContractParameters {
  shielded_TRC20_Parameters?: ShieldedTRC20Parameters | undefined;
  spend_authority_signature?: BytesMessage[] | undefined;
  amount?: string | undefined;
  transparent_to_address?: string | undefined;
}
