/* eslint-disable */
import type { Transaction } from "../core/Tron";

export interface ZksnarkRequest {
  transaction?: Transaction | undefined;
  sighash?: string | undefined;
  valueBalance?: number | undefined;
  txId?: string | undefined;
}

export interface ZksnarkResponse {
  code?: ZksnarkResponse_Code | undefined;
}

export declare const enum ZksnarkResponse_Code {
  SUCCESS = 0,
  FAILED = 1,
}
