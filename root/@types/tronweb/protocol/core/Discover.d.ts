/* eslint-disable */

export interface Endpoint {
  address?: string | undefined;
  port?: number | undefined;
  nodeId?: string | undefined;
}

export interface PingMessage {
  from?: Endpoint | undefined;
  to?: Endpoint | undefined;
  version?: number | undefined;
  timestamp?: number | undefined;
}

export interface PongMessage {
  from?: Endpoint | undefined;
  echo?: number | undefined;
  timestamp?: number | undefined;
}

export interface FindNeighbours {
  from?: Endpoint | undefined;
  targetId?: string | undefined;
  timestamp?: number | undefined;
}

export interface Neighbours {
  from?: Endpoint | undefined;
  neighbours?: Endpoint[] | undefined;
  timestamp?: number | undefined;
}

export interface BackupMessage {
  flag?: boolean | undefined;
  priority?: number | undefined;
}
