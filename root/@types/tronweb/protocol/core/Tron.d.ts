/* eslint-disable */
import type { Any } from "../google/protobuf/any";
import type { Endpoint } from "./Discover";
import type { ResourceCode } from "./contract/common";

export declare const enum AccountType {
  Normal = 0,
  AssetIssue = 1,
  Contract = 2,
}

export declare const enum ReasonCode {
  REQUESTED = 0,
  BAD_PROTOCOL = 2,
  TOO_MANY_PEERS = 4,
  DUPLICATE_PEER = 5,
  INCOMPATIBLE_PROTOCOL = 6,
  RANDOM_ELIMINATION = 7,
  PEER_QUITING = 8,
  UNEXPECTED_IDENTITY = 9,
  LOCAL_IDENTITY = 10,
  PING_TIMEOUT = 11,
  USER_REASON = 16,
  RESET = 17,
  SYNC_FAIL = 18,
  FETCH_FAIL = 19,
  BAD_TX = 20,
  BAD_BLOCK = 21,
  FORKED = 22,
  UNLINKABLE = 23,
  INCOMPATIBLE_VERSION = 24,
  INCOMPATIBLE_CHAIN = 25,
  TIME_OUT = 32,
  CONNECT_FAIL = 33,
  TOO_MANY_PEERS_WITH_SAME_IP = 34,
  LIGHT_NODE_SYNC_FAIL = 35,
  UNKNOWN = 255,
}

/** AccountId, (name, address) use name, (null, address) use address, (name, null) use name, */
export interface AccountId {
  name?: string | undefined;
  address?: string | undefined;
}

/** vote message */
export interface Vote {
  /** the super rep address */
  vote_address?:
    | string
    | undefined;
  /** the vote num to this super rep. */
  vote_count?: number | undefined;
}

/** Proposal */
export interface Proposal {
  proposal_id?: number | undefined;
  proposer_address?: string | undefined;
  parameters?: { [key: number]: number } | undefined;
  expiration_time?: number | undefined;
  create_time?: number | undefined;
  approvals?: string[] | undefined;
  state?: Proposal_State | undefined;
}

export declare const enum Proposal_State {
  PENDING = 0,
  DISAPPROVED = 1,
  APPROVED = 2,
  CANCELED = 3,
}

export interface Proposal_ParametersEntry {
  key: number;
  value: number;
}

/** Exchange */
export interface Exchange {
  exchange_id?: number | undefined;
  creator_address?: string | undefined;
  create_time?: number | undefined;
  first_token_id?: string | undefined;
  first_token_balance?: number | undefined;
  second_token_id?: string | undefined;
  second_token_balance?: number | undefined;
}

/** market */
export interface MarketOrder {
  order_id?: string | undefined;
  owner_address?: string | undefined;
  create_time?: number | undefined;
  sell_token_id?: string | undefined;
  sell_token_quantity?: number | undefined;
  buy_token_id?:
    | string
    | undefined;
  /** min to receive */
  buy_token_quantity?: number | undefined;
  sell_token_quantity_remain?:
    | number
    | undefined;
  /**
   * When state != ACTIVE and sell_token_quantity_return !=0,
   * it means that some sell tokens are returned to the account due to insufficient remaining amount
   */
  sell_token_quantity_return?: number | undefined;
  state?: MarketOrder_State | undefined;
  prev?: string | undefined;
  next?: string | undefined;
}

export declare const enum MarketOrder_State {
  ACTIVE = 0,
  INACTIVE = 1,
  CANCELED = 2,
}

export interface MarketOrderList {
  orders?: MarketOrder[] | undefined;
}

export interface MarketOrderPairList {
  orderPair?: MarketOrderPair[] | undefined;
}

export interface MarketOrderPair {
  sell_token_id?: string | undefined;
  buy_token_id?: string | undefined;
}

export interface MarketAccountOrder {
  owner_address?:
    | string
    | undefined;
  /** order_id list */
  orders?:
    | string[]
    | undefined;
  /** active count */
  count?: number | undefined;
  total_count?: number | undefined;
}

export interface MarketPrice {
  sell_token_quantity?: number | undefined;
  buy_token_quantity?: number | undefined;
}

export interface MarketPriceList {
  sell_token_id?: string | undefined;
  buy_token_id?: string | undefined;
  prices?: MarketPrice[] | undefined;
}

export interface MarketOrderIdList {
  head?: string | undefined;
  tail?: string | undefined;
}

export interface ChainParameters {
  chainParameter?: ChainParameters_ChainParameter[] | undefined;
}

export interface ChainParameters_ChainParameter {
  key?: string | undefined;
  value?: number | undefined;
}

/** Account */
export interface Account {
  /** account nick name */
  account_name?: string | undefined;
  type?:
    | AccountType
    | undefined;
  /** the create address */
  address?:
    | string
    | undefined;
  /** the trx balance */
  balance?:
    | number
    | undefined;
  /** the votes */
  votes?:
    | Vote[]
    | undefined;
  /** the other asset owned by this account */
  asset?:
    | { [key: string]: number }
    | undefined;
  /** the other asset owned by this account，key is assetId */
  assetV2?:
    | { [key: string]: number }
    | undefined;
  /** the frozen balance for bandwidth */
  frozen?:
    | Account_Frozen[]
    | undefined;
  /** bandwidth, get from frozen */
  net_usage?:
    | number
    | undefined;
  /** Frozen balance provided by other accounts to this account */
  acquired_delegated_frozen_balance_for_bandwidth?:
    | number
    | undefined;
  /** Freeze and provide balances to other accounts */
  delegated_frozen_balance_for_bandwidth?: number | undefined;
  old_tron_power?: number | undefined;
  tron_power?: Account_Frozen | undefined;
  asset_optimized?:
    | boolean
    | undefined;
  /** this account create time */
  create_time?:
    | number
    | undefined;
  /** this last operation time, including transfer, voting and so on. //FIXME fix grammar */
  latest_opration_time?:
    | number
    | undefined;
  /** witness block producing allowance */
  allowance?:
    | number
    | undefined;
  /** last withdraw time */
  latest_withdraw_time?:
    | number
    | undefined;
  /** not used so far */
  code?: string | undefined;
  is_witness?: boolean | undefined;
  is_committee?:
    | boolean
    | undefined;
  /** frozen asset(for asset issuer) */
  frozen_supply?:
    | Account_Frozen[]
    | undefined;
  /** asset_issued_name */
  asset_issued_name?: string | undefined;
  asset_issued_ID?: string | undefined;
  latest_asset_operation_time?: { [key: string]: number } | undefined;
  latest_asset_operation_timeV2?: { [key: string]: number } | undefined;
  free_net_usage?: number | undefined;
  free_asset_net_usage?: { [key: string]: number } | undefined;
  free_asset_net_usageV2?: { [key: string]: number } | undefined;
  latest_consume_time?: number | undefined;
  latest_consume_free_time?:
    | number
    | undefined;
  /** the identity of this account, case insensitive */
  account_id?: string | undefined;
  net_window_size?: number | undefined;
  account_resource?: Account_AccountResource | undefined;
  codeHash?: string | undefined;
  owner_permission?: Permission | undefined;
  witness_permission?: Permission | undefined;
  active_permission?: Permission[] | undefined;
  frozenV2?: Account_FreezeV2[] | undefined;
  unfrozenV2?: Account_UnFreezeV2[] | undefined;
  delegated_frozenV2_balance_for_bandwidth?: number | undefined;
  acquired_delegated_frozenV2_balance_for_bandwidth?: number | undefined;
}

/** frozen balance */
export interface Account_Frozen {
  /** the frozen trx balance */
  frozen_balance?:
    | number
    | undefined;
  /** the expire time */
  expire_time?: number | undefined;
}

export interface Account_AssetEntry {
  key: string;
  value: number;
}

export interface Account_AssetV2Entry {
  key: string;
  value: number;
}

export interface Account_LatestAssetOperationTimeEntry {
  key: string;
  value: number;
}

export interface Account_LatestAssetOperationTimeV2Entry {
  key: string;
  value: number;
}

export interface Account_FreeAssetNetUsageEntry {
  key: string;
  value: number;
}

export interface Account_FreeAssetNetUsageV2Entry {
  key: string;
  value: number;
}

export interface Account_AccountResource {
  /** energy resource, get from frozen */
  energy_usage?:
    | number
    | undefined;
  /** the frozen balance for energy */
  frozen_balance_for_energy?: Account_Frozen | undefined;
  latest_consume_time_for_energy?:
    | number
    | undefined;
  /** Frozen balance provided by other accounts to this account */
  acquired_delegated_frozen_balance_for_energy?:
    | number
    | undefined;
  /** Frozen balances provided to other accounts */
  delegated_frozen_balance_for_energy?:
    | number
    | undefined;
  /** storage resource, get from market */
  storage_limit?: number | undefined;
  storage_usage?: number | undefined;
  latest_exchange_storage_time?: number | undefined;
  energy_window_size?: number | undefined;
  delegated_frozenV2_balance_for_energy?: number | undefined;
  acquired_delegated_frozenV2_balance_for_energy?: number | undefined;
}

export interface Account_FreezeV2 {
  type?: ResourceCode | undefined;
  amount?: number | undefined;
}

export interface Account_UnFreezeV2 {
  type?: ResourceCode | undefined;
  unfreeze_amount?: number | undefined;
  unfreeze_expire_time?: number | undefined;
}

export interface Key {
  address?: string | undefined;
  weight?: number | undefined;
}

export interface DelegatedResource {
  from?: string | undefined;
  to?: string | undefined;
  frozen_balance_for_bandwidth?: number | undefined;
  frozen_balance_for_energy?: number | undefined;
  expire_time_for_bandwidth?: number | undefined;
  expire_time_for_energy?: number | undefined;
}

export interface authority {
  account?: AccountId | undefined;
  permission_name?: string | undefined;
}

export interface Permission {
  type?:
    | Permission_PermissionType
    | undefined;
  /** Owner id=0, Witness id=1, Active id start by 2 */
  id?: number | undefined;
  permission_name?: string | undefined;
  threshold?: number | undefined;
  parent_id?:
    | number
    | undefined;
  /** 1 bit 1 contract */
  operations?: string | undefined;
  keys?: Key[] | undefined;
}

export declare const enum Permission_PermissionType {
  Owner = 0,
  Witness = 1,
  Active = 2,
}

/** Witness */
export interface Witness {
  address?: string | undefined;
  voteCount?: number | undefined;
  pubKey?: string | undefined;
  url?: string | undefined;
  totalProduced?: number | undefined;
  totalMissed?: number | undefined;
  latestBlockNum?: number | undefined;
  latestSlotNum?: number | undefined;
  isJobs?: boolean | undefined;
}

/** Vote Change */
export interface Votes {
  address?: string | undefined;
  old_votes?: Vote[] | undefined;
  new_votes?: Vote[] | undefined;
}

export interface TXOutput {
  value?: number | undefined;
  pubKeyHash?: string | undefined;
}

export interface TXInput {
  raw_data?: TXInput_raw | undefined;
  signature?: string | undefined;
}

export interface TXInput_raw {
  txID?: string | undefined;
  vout?: number | undefined;
  pubKey?: string | undefined;
}

export interface TXOutputs {
  outputs?: TXOutput[] | undefined;
}

export interface ResourceReceipt {
  energy_usage?: number | undefined;
  energy_fee?: number | undefined;
  origin_energy_usage?: number | undefined;
  energy_usage_total?: number | undefined;
  net_usage?: number | undefined;
  net_fee?: number | undefined;
  result?: Transaction_Result_contractResult | undefined;
  energy_penalty_total?: number | undefined;
}

export interface MarketOrderDetail {
  makerOrderId?: string | undefined;
  takerOrderId?: string | undefined;
  fillSellQuantity?: number | undefined;
  fillBuyQuantity?: number | undefined;
}

export interface Transaction {
  raw_data?:
    | Transaction_raw
    | undefined;
  /** only support size = 1,  repeated list here for muti-sig extension */
  signature?: string[] | undefined;
  ret?: Transaction_Result[] | undefined;
}

export interface Transaction_Contract {
  type?: Transaction_Contract_ContractType | undefined;
  parameter?: Any | undefined;
  provider?: string | undefined;
  ContractName?: string | undefined;
  Permission_id?: number | undefined;
}

export declare const enum Transaction_Contract_ContractType {
  AccountCreateContract = 0,
  TransferContract = 1,
  TransferAssetContract = 2,
  VoteAssetContract = 3,
  VoteWitnessContract = 4,
  WitnessCreateContract = 5,
  AssetIssueContract = 6,
  WitnessUpdateContract = 8,
  ParticipateAssetIssueContract = 9,
  AccountUpdateContract = 10,
  FreezeBalanceContract = 11,
  UnfreezeBalanceContract = 12,
  WithdrawBalanceContract = 13,
  UnfreezeAssetContract = 14,
  UpdateAssetContract = 15,
  ProposalCreateContract = 16,
  ProposalApproveContract = 17,
  ProposalDeleteContract = 18,
  SetAccountIdContract = 19,
  CustomContract = 20,
  CreateSmartContract = 30,
  TriggerSmartContract = 31,
  GetContract = 32,
  UpdateSettingContract = 33,
  ExchangeCreateContract = 41,
  ExchangeInjectContract = 42,
  ExchangeWithdrawContract = 43,
  ExchangeTransactionContract = 44,
  UpdateEnergyLimitContract = 45,
  AccountPermissionUpdateContract = 46,
  ClearABIContract = 48,
  UpdateBrokerageContract = 49,
  ShieldedTransferContract = 51,
  MarketSellAssetContract = 52,
  MarketCancelOrderContract = 53,
  FreezeBalanceV2Contract = 54,
  UnfreezeBalanceV2Contract = 55,
  WithdrawExpireUnfreezeContract = 56,
  DelegateResourceContract = 57,
  UnDelegateResourceContract = 58,
}

export interface Transaction_Result {
  fee?: number | undefined;
  ret?: Transaction_Result_code | undefined;
  contractRet?: Transaction_Result_contractResult | undefined;
  assetIssueID?: string | undefined;
  withdraw_amount?: number | undefined;
  unfreeze_amount?: number | undefined;
  exchange_received_amount?: number | undefined;
  exchange_inject_another_amount?: number | undefined;
  exchange_withdraw_another_amount?: number | undefined;
  exchange_id?: number | undefined;
  shielded_transaction_fee?: number | undefined;
  orderId?: string | undefined;
  orderDetails?: MarketOrderDetail[] | undefined;
  withdraw_expire_amount?: number | undefined;
}

export declare const enum Transaction_Result_code {
  SUCESS = 0,
  FAILED = 1,
}

export declare const enum Transaction_Result_contractResult {
  DEFAULT = 0,
  SUCCESS = 1,
  REVERT = 2,
  BAD_JUMP_DESTINATION = 3,
  OUT_OF_MEMORY = 4,
  PRECOMPILED_CONTRACT = 5,
  STACK_TOO_SMALL = 6,
  STACK_TOO_LARGE = 7,
  ILLEGAL_OPERATION = 8,
  STACK_OVERFLOW = 9,
  OUT_OF_ENERGY = 10,
  OUT_OF_TIME = 11,
  JVM_STACK_OVER_FLOW = 12,
  UNKNOWN = 13,
  TRANSFER_FAILED = 14,
  INVALID_CODE = 15,
}

export interface Transaction_raw {
  ref_block_bytes?: string | undefined;
  ref_block_num?: number | undefined;
  ref_block_hash?: string | undefined;
  expiration?: number | undefined;
  auths?:
    | authority[]
    | undefined;
  /** data not used */
  data?:
    | string
    | undefined;
  /** only support size = 1,  repeated list here for extension */
  contract?:
    | Transaction_Contract[]
    | undefined;
  /** scripts not used */
  scripts?: string | undefined;
  timestamp?: number | undefined;
  fee_limit?: number | undefined;
}

export interface TransactionInfo {
  id?: string | undefined;
  fee?: number | undefined;
  blockNumber?: number | undefined;
  blockTimeStamp?: number | undefined;
  contractResult?: string[] | undefined;
  contract_address?: string | undefined;
  receipt?: ResourceReceipt | undefined;
  log?: TransactionInfo_Log[] | undefined;
  result?: TransactionInfo_code | undefined;
  resMessage?: string | undefined;
  assetIssueID?: string | undefined;
  withdraw_amount?: number | undefined;
  unfreeze_amount?: number | undefined;
  internal_transactions?: InternalTransaction[] | undefined;
  exchange_received_amount?: number | undefined;
  exchange_inject_another_amount?: number | undefined;
  exchange_withdraw_another_amount?: number | undefined;
  exchange_id?: number | undefined;
  shielded_transaction_fee?: number | undefined;
  orderId?: string | undefined;
  orderDetails?: MarketOrderDetail[] | undefined;
  packingFee?: number | undefined;
  withdraw_expire_amount?: number | undefined;
}

export declare const enum TransactionInfo_code {
  SUCESS = 0,
  FAILED = 1,
}

export interface TransactionInfo_Log {
  address?: string | undefined;
  topics?: string[] | undefined;
  data?: string | undefined;
}

export interface TransactionRet {
  blockNumber?: number | undefined;
  blockTimeStamp?: number | undefined;
  transactioninfo?: TransactionInfo[] | undefined;
}

export interface Transactions {
  transactions?: Transaction[] | undefined;
}

export interface BlockHeader {
  raw_data?: BlockHeader_raw | undefined;
  witness_signature?: string | undefined;
}

export interface BlockHeader_raw {
  timestamp?: number | undefined;
  txTrieRoot?: string | undefined;
  parentHash?:
    | string
    | undefined;
  /**
   * bytes nonce = 5;
   * bytes difficulty = 6;
   */
  number?: number | undefined;
  witness_id?: number | undefined;
  witness_address?: string | undefined;
  version?: number | undefined;
  accountStateRoot?: string | undefined;
}

/** block */
export interface Block {
  transactions?: Transaction[] | undefined;
  block_header?: BlockHeader | undefined;
}

export interface ChainInventory {
  ids?: ChainInventory_BlockId[] | undefined;
  remain_num?: number | undefined;
}

export interface ChainInventory_BlockId {
  hash?: string | undefined;
  number?: number | undefined;
}

/** Inventory */
export interface BlockInventory {
  ids?: BlockInventory_BlockId[] | undefined;
  type?: BlockInventory_Type | undefined;
}

export declare const enum BlockInventory_Type {
  SYNC = 0,
  ADVTISE = 1,
  FETCH = 2,
}

export interface BlockInventory_BlockId {
  hash?: string | undefined;
  number?: number | undefined;
}

export interface Inventory {
  type?: Inventory_InventoryType | undefined;
  ids?: string[] | undefined;
}

export declare const enum Inventory_InventoryType {
  TRX = 0,
  BLOCK = 1,
}

export interface Items {
  type?: Items_ItemType | undefined;
  blocks?: Block[] | undefined;
  block_headers?: BlockHeader[] | undefined;
  transactions?: Transaction[] | undefined;
}

export declare const enum Items_ItemType {
  ERR = 0,
  TRX = 1,
  BLOCK = 2,
  BLOCKHEADER = 3,
}

/** DynamicProperties */
export interface DynamicProperties {
  last_solidity_block_num?: number | undefined;
}

export interface DisconnectMessage {
  reason?: ReasonCode | undefined;
}

export interface HelloMessage {
  from?: Endpoint | undefined;
  version?: number | undefined;
  timestamp?: number | undefined;
  genesisBlockId?: HelloMessage_BlockId | undefined;
  solidBlockId?: HelloMessage_BlockId | undefined;
  headBlockId?: HelloMessage_BlockId | undefined;
  address?: string | undefined;
  signature?: string | undefined;
  nodeType?: number | undefined;
  lowestBlockNum?: number | undefined;
}

export interface HelloMessage_BlockId {
  hash?: string | undefined;
  number?: number | undefined;
}

export interface InternalTransaction {
  /**
   * internalTransaction identity, the root InternalTransaction hash
   * should equals to root transaction id.
   */
  hash?:
    | string
    | undefined;
  /** the one send trx (TBD: or token) via function */
  caller_address?:
    | string
    | undefined;
  /** the one recieve trx (TBD: or token) via function */
  transferTo_address?: string | undefined;
  callValueInfo?: InternalTransaction_CallValueInfo[] | undefined;
  note?: string | undefined;
  rejected?: boolean | undefined;
  extra?: string | undefined;
}

export interface InternalTransaction_CallValueInfo {
  /** trx (TBD: or token) value */
  callValue?:
    | number
    | undefined;
  /** TBD: tokenName, trx should be empty */
  tokenId?: string | undefined;
}

export interface DelegatedResourceAccountIndex {
  account?: string | undefined;
  fromAccounts?: string[] | undefined;
  toAccounts?: string[] | undefined;
  timestamp?: number | undefined;
}

export interface NodeInfo {
  beginSyncNum?: number | undefined;
  block?: string | undefined;
  solidityBlock?:
    | string
    | undefined;
  /** connect information */
  currentConnectCount?: number | undefined;
  activeConnectCount?: number | undefined;
  passiveConnectCount?: number | undefined;
  totalFlow?: number | undefined;
  peerInfoList?: NodeInfo_PeerInfo[] | undefined;
  configNodeInfo?: NodeInfo_ConfigNodeInfo | undefined;
  machineInfo?: NodeInfo_MachineInfo | undefined;
  cheatWitnessInfoMap?: { [key: string]: string } | undefined;
}

export interface NodeInfo_CheatWitnessInfoMapEntry {
  key: string;
  value: string;
}

export interface NodeInfo_PeerInfo {
  lastSyncBlock?: string | undefined;
  remainNum?: number | undefined;
  lastBlockUpdateTime?: number | undefined;
  syncFlag?: boolean | undefined;
  headBlockTimeWeBothHave?: number | undefined;
  needSyncFromPeer?: boolean | undefined;
  needSyncFromUs?: boolean | undefined;
  host?: string | undefined;
  port?: number | undefined;
  nodeId?: string | undefined;
  connectTime?: number | undefined;
  avgLatency?: number | undefined;
  syncToFetchSize?: number | undefined;
  syncToFetchSizePeekNum?: number | undefined;
  syncBlockRequestedSize?: number | undefined;
  unFetchSynNum?: number | undefined;
  blockInPorcSize?: number | undefined;
  headBlockWeBothHave?: string | undefined;
  isActive?: boolean | undefined;
  score?: number | undefined;
  nodeCount?: number | undefined;
  inFlow?: number | undefined;
  disconnectTimes?: number | undefined;
  localDisconnectReason?: string | undefined;
  remoteDisconnectReason?: string | undefined;
}

export interface NodeInfo_ConfigNodeInfo {
  codeVersion?: string | undefined;
  p2pVersion?: string | undefined;
  listenPort?: number | undefined;
  discoverEnable?: boolean | undefined;
  activeNodeSize?: number | undefined;
  passiveNodeSize?: number | undefined;
  sendNodeSize?: number | undefined;
  maxConnectCount?: number | undefined;
  sameIpMaxConnectCount?: number | undefined;
  backupListenPort?: number | undefined;
  backupMemberSize?: number | undefined;
  backupPriority?: number | undefined;
  dbVersion?: number | undefined;
  minParticipationRate?: number | undefined;
  supportConstant?: boolean | undefined;
  minTimeRatio?: number | undefined;
  maxTimeRatio?: number | undefined;
  allowCreationOfContracts?: number | undefined;
  allowAdaptiveEnergy?: number | undefined;
}

export interface NodeInfo_MachineInfo {
  threadCount?: number | undefined;
  deadLockThreadCount?: number | undefined;
  cpuCount?: number | undefined;
  totalMemory?: number | undefined;
  freeMemory?: number | undefined;
  cpuRate?: number | undefined;
  javaVersion?: string | undefined;
  osName?: string | undefined;
  jvmTotalMemory?: number | undefined;
  jvmFreeMemory?: number | undefined;
  processCpuRate?: number | undefined;
  memoryDescInfoList?: NodeInfo_MachineInfo_MemoryDescInfo[] | undefined;
  deadLockThreadInfoList?: NodeInfo_MachineInfo_DeadLockThreadInfo[] | undefined;
}

export interface NodeInfo_MachineInfo_MemoryDescInfo {
  name?: string | undefined;
  initSize?: number | undefined;
  useSize?: number | undefined;
  maxSize?: number | undefined;
  useRate?: number | undefined;
}

export interface NodeInfo_MachineInfo_DeadLockThreadInfo {
  name?: string | undefined;
  lockName?: string | undefined;
  lockOwner?: string | undefined;
  state?: string | undefined;
  blockTime?: number | undefined;
  waitTime?: number | undefined;
  stackTrace?: string | undefined;
}

export interface MetricsInfo {
  interval?: number | undefined;
  node?: MetricsInfo_NodeInfo | undefined;
  blockchain?: MetricsInfo_BlockChainInfo | undefined;
  net?: MetricsInfo_NetInfo | undefined;
}

export interface MetricsInfo_NodeInfo {
  ip?: string | undefined;
  nodeType?: number | undefined;
  version?: string | undefined;
  backupStatus?: number | undefined;
}

export interface MetricsInfo_BlockChainInfo {
  headBlockNum?: number | undefined;
  headBlockTimestamp?: number | undefined;
  headBlockHash?: string | undefined;
  forkCount?: number | undefined;
  failForkCount?: number | undefined;
  blockProcessTime?: MetricsInfo_RateInfo | undefined;
  tps?: MetricsInfo_RateInfo | undefined;
  transactionCacheSize?: number | undefined;
  missedTransaction?: MetricsInfo_RateInfo | undefined;
  witnesses?: MetricsInfo_BlockChainInfo_Witness[] | undefined;
  failProcessBlockNum?: number | undefined;
  failProcessBlockReason?: string | undefined;
  dupWitness?: MetricsInfo_BlockChainInfo_DupWitness[] | undefined;
}

export interface MetricsInfo_BlockChainInfo_Witness {
  address?: string | undefined;
  version?: number | undefined;
}

export interface MetricsInfo_BlockChainInfo_DupWitness {
  address?: string | undefined;
  blockNum?: number | undefined;
  count?: number | undefined;
}

export interface MetricsInfo_RateInfo {
  count?: number | undefined;
  meanRate?: number | undefined;
  oneMinuteRate?: number | undefined;
  fiveMinuteRate?: number | undefined;
  fifteenMinuteRate?: number | undefined;
}

export interface MetricsInfo_NetInfo {
  errorProtoCount?: number | undefined;
  api?: MetricsInfo_NetInfo_ApiInfo | undefined;
  connectionCount?: number | undefined;
  validConnectionCount?: number | undefined;
  tcpInTraffic?: MetricsInfo_RateInfo | undefined;
  tcpOutTraffic?: MetricsInfo_RateInfo | undefined;
  disconnectionCount?: number | undefined;
  disconnectionDetail?: MetricsInfo_NetInfo_DisconnectionDetailInfo[] | undefined;
  udpInTraffic?: MetricsInfo_RateInfo | undefined;
  udpOutTraffic?: MetricsInfo_RateInfo | undefined;
  latency?: MetricsInfo_NetInfo_LatencyInfo | undefined;
}

export interface MetricsInfo_NetInfo_ApiInfo {
  qps?: MetricsInfo_RateInfo | undefined;
  failQps?: MetricsInfo_RateInfo | undefined;
  outTraffic?: MetricsInfo_RateInfo | undefined;
  detail?: MetricsInfo_NetInfo_ApiInfo_ApiDetailInfo[] | undefined;
}

export interface MetricsInfo_NetInfo_ApiInfo_ApiDetailInfo {
  name?: string | undefined;
  qps?: MetricsInfo_RateInfo | undefined;
  failQps?: MetricsInfo_RateInfo | undefined;
  outTraffic?: MetricsInfo_RateInfo | undefined;
}

export interface MetricsInfo_NetInfo_DisconnectionDetailInfo {
  reason?: string | undefined;
  count?: number | undefined;
}

export interface MetricsInfo_NetInfo_LatencyInfo {
  top99?: number | undefined;
  top95?: number | undefined;
  top75?: number | undefined;
  totalCount?: number | undefined;
  delay1S?: number | undefined;
  delay2S?: number | undefined;
  delay3S?: number | undefined;
  detail?: MetricsInfo_NetInfo_LatencyInfo_LatencyDetailInfo[] | undefined;
}

export interface MetricsInfo_NetInfo_LatencyInfo_LatencyDetailInfo {
  witness?: string | undefined;
  top99?: number | undefined;
  top95?: number | undefined;
  top75?: number | undefined;
  count?: number | undefined;
  delay1S?: number | undefined;
  delay2S?: number | undefined;
  delay3S?: number | undefined;
}

export interface PBFTMessage {
  raw_data?: PBFTMessage_Raw | undefined;
  signature?: string | undefined;
}

export declare const enum PBFTMessage_MsgType {
  VIEW_CHANGE = 0,
  REQUEST = 1,
  PREPREPARE = 2,
  PREPARE = 3,
  COMMIT = 4,
}

export declare const enum PBFTMessage_DataType {
  BLOCK = 0,
  SRL = 1,
}

export interface PBFTMessage_Raw {
  msg_type?: PBFTMessage_MsgType | undefined;
  data_type?: PBFTMessage_DataType | undefined;
  view_n?: number | undefined;
  epoch?: number | undefined;
  data?: string | undefined;
}

export interface PBFTCommitResult {
  data?: string | undefined;
  signature?: string[] | undefined;
}

export interface SRL {
  srAddress?: string[] | undefined;
}
