/* eslint-disable */
import type { AccountType, Permission } from "../Tron";

export interface AccountCreateContract {
  owner_address?: string | undefined;
  account_address?: string | undefined;
  type?: AccountType | undefined;
}

/** Update account name. Account name is not unique now. */
export interface AccountUpdateContract {
  account_name?: string | undefined;
  owner_address?: string | undefined;
}

/** Set account id if the account has no id. Account id is unique and case insensitive. */
export interface SetAccountIdContract {
  account_id?: string | undefined;
  owner_address?: string | undefined;
}

export interface AccountPermissionUpdateContract {
  owner_address?:
    | string
    | undefined;
  /** Empty is invalidate */
  owner?:
    | Permission
    | undefined;
  /** Can be empty */
  witness?:
    | Permission
    | undefined;
  /** Empty is invalidate */
  actives?: Permission[] | undefined;
}
