/* eslint-disable */

export interface VoteAssetContract {
  owner_address?: string | undefined;
  vote_address?: string[] | undefined;
  support?: boolean | undefined;
  count?: number | undefined;
}
