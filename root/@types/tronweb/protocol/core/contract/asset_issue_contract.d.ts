/* eslint-disable */

export interface AssetIssueContract {
  id?: string | undefined;
  owner_address?: string | undefined;
  name?: string | undefined;
  abbr?: string | undefined;
  total_supply?: number | undefined;
  frozen_supply?: AssetIssueContract_FrozenSupply[] | undefined;
  trx_num?: number | undefined;
  precision?: number | undefined;
  num?: number | undefined;
  start_time?: number | undefined;
  end_time?:
    | number
    | undefined;
  /** useless */
  order?: number | undefined;
  vote_score?: number | undefined;
  description?: string | undefined;
  url?: string | undefined;
  free_asset_net_limit?: number | undefined;
  public_free_asset_net_limit?: number | undefined;
  public_free_asset_net_usage?: number | undefined;
  public_latest_free_net_time?: number | undefined;
}

export interface AssetIssueContract_FrozenSupply {
  frozen_amount?: number | undefined;
  frozen_days?: number | undefined;
}

export interface TransferAssetContract {
  /** this field is token name before the proposal ALLOW_SAME_TOKEN_NAME is active, otherwise it is token id and token is should be in string format. */
  asset_name?: string | undefined;
  owner_address?: string | undefined;
  to_address?: string | undefined;
  amount?: number | undefined;
}

export interface UnfreezeAssetContract {
  owner_address?: string | undefined;
}

export interface UpdateAssetContract {
  owner_address?: string | undefined;
  description?: string | undefined;
  url?: string | undefined;
  new_limit?: number | undefined;
  new_public_limit?: number | undefined;
}

export interface ParticipateAssetIssueContract {
  owner_address?: string | undefined;
  to_address?:
    | string
    | undefined;
  /** this field is token name before the proposal ALLOW_SAME_TOKEN_NAME is active, otherwise it is token id and token is should be in string format. */
  asset_name?:
    | string
    | undefined;
  /** the amount of drops */
  amount?: number | undefined;
}
