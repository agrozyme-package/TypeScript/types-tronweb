/* eslint-disable */

export interface ExchangeCreateContract {
  owner_address?: string | undefined;
  first_token_id?: string | undefined;
  first_token_balance?: number | undefined;
  second_token_id?: string | undefined;
  second_token_balance?: number | undefined;
}

export interface ExchangeInjectContract {
  owner_address?: string | undefined;
  exchange_id?: number | undefined;
  token_id?: string | undefined;
  quant?: number | undefined;
}

export interface ExchangeWithdrawContract {
  owner_address?: string | undefined;
  exchange_id?: number | undefined;
  token_id?: string | undefined;
  quant?: number | undefined;
}

export interface ExchangeTransactionContract {
  owner_address?: string | undefined;
  exchange_id?: number | undefined;
  token_id?: string | undefined;
  quant?: number | undefined;
  expected?: number | undefined;
}
