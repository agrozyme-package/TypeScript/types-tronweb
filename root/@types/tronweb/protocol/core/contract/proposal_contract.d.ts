/* eslint-disable */

export interface ProposalApproveContract {
  owner_address?: string | undefined;
  proposal_id?:
    | number
    | undefined;
  /** add or remove approval */
  is_add_approval?: boolean | undefined;
}

export interface ProposalCreateContract {
  owner_address?: string | undefined;
  parameters?: { [key: number]: number } | undefined;
}

export interface ProposalCreateContract_ParametersEntry {
  key: number;
  value: number;
}

export interface ProposalDeleteContract {
  owner_address?: string | undefined;
  proposal_id?: number | undefined;
}
