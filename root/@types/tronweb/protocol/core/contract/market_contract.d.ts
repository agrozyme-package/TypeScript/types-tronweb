/* eslint-disable */

export interface MarketSellAssetContract {
  owner_address?: string | undefined;
  sell_token_id?: string | undefined;
  sell_token_quantity?: number | undefined;
  buy_token_id?:
    | string
    | undefined;
  /** min to receive */
  buy_token_quantity?: number | undefined;
}

export interface MarketCancelOrderContract {
  owner_address?: string | undefined;
  order_id?: string | undefined;
}
