/* eslint-disable */

export interface BuyStorageBytesContract {
  owner_address?:
    | string
    | undefined;
  /** storage bytes for buy */
  bytes?: number | undefined;
}

export interface BuyStorageContract {
  owner_address?:
    | string
    | undefined;
  /** trx quantity for buy storage (sun) */
  quant?: number | undefined;
}

export interface SellStorageContract {
  owner_address?: string | undefined;
  storage_bytes?: number | undefined;
}

export interface UpdateBrokerageContract {
  owner_address?:
    | string
    | undefined;
  /** 1 mean 1% */
  brokerage?: number | undefined;
}
