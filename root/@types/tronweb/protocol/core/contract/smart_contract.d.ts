/* eslint-disable */

export interface SmartContract {
  origin_address?: string | undefined;
  contract_address?: string | undefined;
  abi?: SmartContract_ABI | undefined;
  bytecode?: string | undefined;
  call_value?: number | undefined;
  consume_user_resource_percent?: number | undefined;
  name?: string | undefined;
  origin_energy_limit?: number | undefined;
  code_hash?: string | undefined;
  trx_hash?: string | undefined;
  version?: number | undefined;
}

export interface SmartContract_ABI {
  entrys?: SmartContract_ABI_Entry[] | undefined;
}

export interface SmartContract_ABI_Entry {
  anonymous?: boolean | undefined;
  constant?: boolean | undefined;
  name?: string | undefined;
  inputs?: SmartContract_ABI_Entry_Param[] | undefined;
  outputs?: SmartContract_ABI_Entry_Param[] | undefined;
  type?: SmartContract_ABI_Entry_EntryType | undefined;
  payable?: boolean | undefined;
  stateMutability?: SmartContract_ABI_Entry_StateMutabilityType | undefined;
}

export declare const enum SmartContract_ABI_Entry_EntryType {
  UnknownEntryType = 0,
  Constructor = 1,
  Function = 2,
  Event = 3,
  Fallback = 4,
  Receive = 5,
  Error = 6,
}

export declare const enum SmartContract_ABI_Entry_StateMutabilityType {
  UnknownMutabilityType = 0,
  Pure = 1,
  View = 2,
  Nonpayable = 3,
  Payable = 4,
}

export interface SmartContract_ABI_Entry_Param {
  indexed?: boolean | undefined;
  name?:
    | string
    | undefined;
  /** SolidityType type = 3; */
  type?: string | undefined;
}

export interface ContractState {
  energy_usage?: number | undefined;
  energy_factor?: number | undefined;
  update_cycle?: number | undefined;
}

export interface CreateSmartContract {
  owner_address?: string | undefined;
  new_contract?: SmartContract | undefined;
  call_token_value?: number | undefined;
  token_id?: number | undefined;
}

export interface TriggerSmartContract {
  owner_address?: string | undefined;
  contract_address?: string | undefined;
  call_value?: number | undefined;
  data?: string | undefined;
  call_token_value?: number | undefined;
  token_id?: number | undefined;
}

export interface ClearABIContract {
  owner_address?: string | undefined;
  contract_address?: string | undefined;
}

export interface UpdateSettingContract {
  owner_address?: string | undefined;
  contract_address?: string | undefined;
  consume_user_resource_percent?: number | undefined;
}

export interface UpdateEnergyLimitContract {
  owner_address?: string | undefined;
  contract_address?: string | undefined;
  origin_energy_limit?: number | undefined;
}

export interface SmartContractDataWrapper {
  smart_contract?: SmartContract | undefined;
  runtimecode?: string | undefined;
  contract_state?: ContractState | undefined;
}
