/* eslint-disable */

export interface WitnessCreateContract {
  owner_address?: string | undefined;
  url?: string | undefined;
}

export interface WitnessUpdateContract {
  owner_address?: string | undefined;
  update_url?: string | undefined;
}

export interface VoteWitnessContract {
  owner_address?: string | undefined;
  votes?: VoteWitnessContract_Vote[] | undefined;
  support?: boolean | undefined;
}

export interface VoteWitnessContract_Vote {
  vote_address?: string | undefined;
  vote_count?: number | undefined;
}
