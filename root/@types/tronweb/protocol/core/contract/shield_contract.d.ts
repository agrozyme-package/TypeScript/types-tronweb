/* eslint-disable */

export interface AuthenticationPath {
  value?: boolean[] | undefined;
}

export interface MerklePath {
  authentication_paths?: AuthenticationPath[] | undefined;
  index?: boolean[] | undefined;
  rt?: string | undefined;
}

export interface OutputPoint {
  hash?: string | undefined;
  index?: number | undefined;
}

export interface OutputPointInfo {
  out_points?: OutputPoint[] | undefined;
  block_num?: number | undefined;
}

export interface PedersenHash {
  content?: string | undefined;
}

export interface IncrementalMerkleTree {
  left?: PedersenHash | undefined;
  right?: PedersenHash | undefined;
  parents?: PedersenHash[] | undefined;
}

export interface IncrementalMerkleVoucher {
  tree?: IncrementalMerkleTree | undefined;
  filled?: PedersenHash[] | undefined;
  cursor?: IncrementalMerkleTree | undefined;
  cursor_depth?: number | undefined;
  rt?: string | undefined;
  output_point?: OutputPoint | undefined;
}

export interface IncrementalMerkleVoucherInfo {
  vouchers?: IncrementalMerkleVoucher[] | undefined;
  paths?: string[] | undefined;
}

export interface SpendDescription {
  value_commitment?:
    | string
    | undefined;
  /** merkle root */
  anchor?:
    | string
    | undefined;
  /** used for check double spend */
  nullifier?:
    | string
    | undefined;
  /** used for check spend authority signature */
  rk?: string | undefined;
  zkproof?: string | undefined;
  spend_authority_signature?: string | undefined;
}

export interface ReceiveDescription {
  value_commitment?: string | undefined;
  note_commitment?:
    | string
    | undefined;
  /** for Encryption */
  epk?:
    | string
    | undefined;
  /** Encryption for incoming, decrypt it with ivk */
  c_enc?:
    | string
    | undefined;
  /** Encryption for audit, decrypt it with ovk */
  c_out?: string | undefined;
  zkproof?: string | undefined;
}

export interface ShieldedTransferContract {
  /** transparent address */
  transparent_from_address?: string | undefined;
  from_amount?: number | undefined;
  spend_description?: SpendDescription[] | undefined;
  receive_description?: ReceiveDescription[] | undefined;
  binding_signature?:
    | string
    | undefined;
  /** transparent address */
  transparent_to_address?:
    | string
    | undefined;
  /** the amount to transparent to_address */
  to_amount?: number | undefined;
}
