/* eslint-disable */
import type { ResourceCode } from "./common";

export interface FreezeBalanceContract {
  owner_address?: string | undefined;
  frozen_balance?: number | undefined;
  frozen_duration?: number | undefined;
  resource?: ResourceCode | undefined;
  receiver_address?: string | undefined;
}

export interface UnfreezeBalanceContract {
  owner_address?: string | undefined;
  resource?: ResourceCode | undefined;
  receiver_address?: string | undefined;
}

export interface WithdrawBalanceContract {
  owner_address?: string | undefined;
}

export interface TransferContract {
  owner_address?: string | undefined;
  to_address?: string | undefined;
  amount?: number | undefined;
}

export interface TransactionBalanceTrace {
  transaction_identifier?: string | undefined;
  operation?: TransactionBalanceTrace_Operation[] | undefined;
  type?: string | undefined;
  status?: string | undefined;
}

export interface TransactionBalanceTrace_Operation {
  operation_identifier?: number | undefined;
  address?: string | undefined;
  amount?: number | undefined;
}

export interface BlockBalanceTrace {
  block_identifier?: BlockBalanceTrace_BlockIdentifier | undefined;
  timestamp?:
    | number
    | undefined;
  /** BlockIdentifier parent_block_identifier = 4; */
  transaction_balance_trace?: TransactionBalanceTrace[] | undefined;
}

export interface BlockBalanceTrace_BlockIdentifier {
  hash?: string | undefined;
  number?: number | undefined;
}

export interface AccountTrace {
  balance?: number | undefined;
  placeholder?: number | undefined;
}

export interface AccountIdentifier {
  address?: string | undefined;
}

export interface AccountBalanceRequest {
  account_identifier?: AccountIdentifier | undefined;
  block_identifier?: BlockBalanceTrace_BlockIdentifier | undefined;
}

export interface AccountBalanceResponse {
  balance?: number | undefined;
  block_identifier?: BlockBalanceTrace_BlockIdentifier | undefined;
}

export interface FreezeBalanceV2Contract {
  owner_address?: string | undefined;
  frozen_balance?: number | undefined;
  resource?: ResourceCode | undefined;
}

export interface UnfreezeBalanceV2Contract {
  owner_address?: string | undefined;
  unfreeze_balance?: number | undefined;
  resource?: ResourceCode | undefined;
}

export interface WithdrawExpireUnfreezeContract {
  owner_address?: string | undefined;
}

export interface DelegateResourceContract {
  owner_address?: string | undefined;
  resource?: ResourceCode | undefined;
  balance?: number | undefined;
  receiver_address?: string | undefined;
  lock?: boolean | undefined;
}

export interface UnDelegateResourceContract {
  owner_address?: string | undefined;
  resource?: ResourceCode | undefined;
  balance?: number | undefined;
  receiver_address?: string | undefined;
}
