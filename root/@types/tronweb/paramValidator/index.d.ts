import TronWeb from '../index';
import { ValidatorParameter } from '../types';

export declare class Validator {
  tronWeb: TronWeb;

  constructor(tronWeb?: TronWeb);

  invalid(param: ValidatorParameter): string;

  notPositive(param: ValidatorParameter): string;

  notEqual(param: ValidatorParameter): string;

  notValid(params: ValidatorParameter[] = [], callback?: Function);
}

export default Validator;
