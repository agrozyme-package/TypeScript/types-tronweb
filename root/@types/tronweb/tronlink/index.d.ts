import TronWeb from '../index';
import { SideChain } from '../lib';

//export type InjectionRequestArgument = { method: string; params?: any };
//export type InjectionRequest = <T>(argument: InjectionRequestArgument) => Promise<T>;

abstract class TronLinkBase {
  request: <T>(argument: { method: string; params?: any }) => Promise<T>;
  tronWeb: TronWeb | false;
  tronlinkParams: {
    websiteIcon: string;
    websiteName: string;
  };
}

export declare class Tron extends TronLinkBase {
  isTronLink: boolean;
}

export declare class TronLink extends TronLinkBase {
  ready: boolean;
  sunWeb: SideChain | false;
}

export * from './event';
export * from './request';
