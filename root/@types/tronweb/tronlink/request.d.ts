export type TronLinkAssetType = 'trc10' | 'trc20' | 'trc721';

export interface TronLinkWatchAssetParameter {
  type: TronLinkAssetType;
  options: {
    address: string;
    symbol?: string;
    decimals?: number;
    image?: string;
  };
}
