export type TronLinkEventType = 'connect' | 'disconnect' | 'accountsChanged' | 'setNode';

export interface TronLinkMessageData<T = {}> {
  isTronLink: boolean;
  message: {
    action: TronLinkEventType;
    data: T;
  };
}

export type TronLinkListener = (event: MessageEvent<TronLinkMessageData>) => Promise<void>;

export interface TronLinkEventAccountsChangedData {
  address: string | false;
}

export interface TronLinkConnectNode {
  eventServer: string;
  fullNode: string;
  solidityNode: string;
  chain: string;
}

export interface TronLinkEventSetNodeData {
  connectNode?: TronLinkConnectNode;
  node: TronLinkConnectNode & { chainId: string };
}

export interface TronLinkEventMap {
  connect?: (data: any) => Promise<void>;
  disconnect?: (data: any) => Promise<void>;
  accountsChanged?: (data: TronLinkEventAccountsChangedData) => Promise<void>;
  setNode?: (data: TronLinkEventSetNodeData) => Promise<void>;
}
