import { JsonFragment } from '@ethersproject/abi';
import BigNumber from 'bignumber.js';
import EventEmitter from 'eventemitter3';
import injectpromise from 'injectpromise';
import { Contract, Event, HttpProvider, Plugin, TransactionBuilder, Trx } from './lib';
import providers from './lib/providers';
import {
  Account,
  AccountWithMnemonic,
  BlockID,
  EventOptions,
  EventRawOptions,
  EventRawResult,
  EventResult,
  HttpProviderHeaders,
  SideChainOptions,
  TronWebNodeServers,
  TronWebOptions,
  TronWebProvider,
} from './types';
import utils from './utils';

interface Address {
  fromHex(address: string): string;
  toHex(address: string): string;
  fromPrivateKey(privateKey: string, strict?: boolean): string | false;
}

export declare class TronWeb extends EventEmitter implements TronWebNodeServers {
  static providers: providers;
  static BigNumber: typeof BigNumber;
  static TransactionBuilder: typeof TransactionBuilder;
  static Trx: typeof Trx;
  static Contract: typeof Contract;
  static Plugin: typeof Plugin;
  static Event: typeof Event;
  static version: string;
  static utils: typeof utils;
  static address: Address;

  event: Event;
  transactionBuilder: TransactionBuilder;
  trx: Trx;
  plugin: Plugin;
  utils: typeof utils;
  providers: providers;
  BigNumber: typeof BigNumber;
  defaultBlock: number | false;
  defaultPrivateKey: string | false;
  defaultAddress: { hex: string | false; base58: string | false };
  address: Address;
  version: string;
  fullnodeVersion: string;
  feeLimit: number;
  injectPromise: ReturnType<typeof injectpromise>;
  fullNode: HttpProvider;
  solidityNode: HttpProvider;
  eventServer: HttpProvider;

  constructor(
    options: TronWebOptions | string,
    solidityNode?: TronWebProvider,
    eventServer?: TronWebProvider,
    sideOptions?: SideChainOptions | string,
    privateKey?: string
  );

  static sha3(string: string, prefix?: boolean): string;

  static toHex(val: any): string;

  static toUtf8(hex: string): string;

  static fromUtf8(string: string): string;

  static toAscii(hex: string): string;

  static fromAscii(string: string, padding: number): string;

  static toDecimal(value: BigNumber.Value): number;

  static fromDecimal(value: BigNumber.Value): string;

  static fromSun(sun: BigNumber): BigNumber;
  static fromSun(sun: string | number): string;

  static toSun(trx: BigNumber): BigNumber;
  static toSun(trx: string | number): string;

  static toBigNumber(amount?: BigNumber.Value): BigNumber;

  static isAddress(address?: string): boolean;

  static createAccount(): Promise<Account>;

  static createRandom(options): AccountWithMnemonic;

  static fromMnemonic(mnemonic: string, path?: string, wordlist?: string): AccountWithMnemonic;

  getFullnodeVersion(): Promise<string>;

  setDefaultBlock(blockID?: BlockID);

  setPrivateKey(privateKey: string);

  setAddress(address: string);

  fullnodeSatisfies(version: string): boolean;

  isValidProvider(provider: any): boolean;

  setFullNode(fullNode: TronWebProvider);

  setSolidityNode(solidityNode: TronWebProvider);

  setEventServer(eventServer?: TronWebProvider, healthcheck?: string);

  setHeader(headers?: HttpProviderHeaders);

  setEventHeader(headers?: HttpProviderHeaders);

  currentProviders(): TronWebNodeServers;

  currentProvider(): TronWebNodeServers;

  getEventResult<T>(contractAddress: string, options: EventRawOptions): Promise<EventRawResult<T>[]>;
  getEventResult<T>(contractAddress: string, options?: EventOptions): Promise<EventResult<T>[]>;

  getEventByTransactionID<T>(transactionID: string, options: EventRawOptions): Promise<EventRawResult<T>[]>;
  getEventByTransactionID<T>(transactionID: string, options?: EventOptions): Promise<EventResult<T>[]>;

  contract(abi?: JsonFragment[], address?: string): Contract;

  sha3(string: string, prefix?: boolean): string;

  toHex(val: any): string;

  toUtf8(hex: string): string;

  fromUtf8(string: string): string;

  toAscii(hex: string): string;

  fromAscii(string: string, padding: number): string;

  toDecimal(value: BigNumber.Value): number;

  fromDecimal(value: BigNumber.Value): string;

  fromSun(sun: BigNumber): BigNumber;
  fromSun(sun: string | number): string;

  toSun(trx: BigNumber): BigNumber;
  toSun(trx: string | number): string;

  toBigNumber(amount?: BigNumber.Value): BigNumber;

  isAddress(address?: string): boolean;

  createAccount(): Promise<Account>;

  createRandom(options): AccountWithMnemonic;

  fromMnemonic(mnemonic: string, path?: string, wordlist?: string): AccountWithMnemonic;

  isConnected(): Promise<Record<keyof TronWebNodeServers, boolean>>;
}

export default TronWeb;

export * from './utility';
export * from './types';

export * from './lib';
export * from './paramValidator';
export * from './proto';
export * from './tronlink';
