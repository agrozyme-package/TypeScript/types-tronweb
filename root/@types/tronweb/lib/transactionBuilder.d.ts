import injectpromise from 'injectpromise';
import TronWeb, { DeployConstantContractOptions, TronEstimateEnergyMessage } from '../index';
import Validator from '../paramValidator';
import {
  TronPermissionPermissionType,
  TronProposalParametersEntry,
  TronTransaction,
  TronTransactionExtention,
} from '../proto';
import {
  AlterTransactionOptions,
  ContractCreateOptions,
  CreateTokenOptions,
  MethodCallOptions,
  MethodSendOptions,
  NewTransactionIdentityOptions,
  Permissions,
  TransactionDataFormat,
  TransactionOptions,
  TransactionResource,
  UpdateTokenOptions,
} from '../types';

export declare class TransactionBuilder {
  tronWeb: TronWeb;
  injectPromise: ReturnType<typeof injectpromise>;
  validator: Validator;

  constructor(tronWeb: TronWeb);

  sendTrx(to: string, amount: number, from?: string, options?: TransactionOptions): Promise<TronTransaction>;

  sendToken(
    to: string,
    amount: number,
    tokenID: string,
    from?: string,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  purchaseToken(
    issuerAddress: string,
    tokenID: string,
    amount: number,
    buyer?: string,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  freezeBalance(
    amount: number,
    duration?: number,
    resource?: TransactionResource,
    address?: string,
    receiverAddress?: string,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  unfreezeBalance(
    resource?: TransactionResource,
    address?: string,
    receiverAddress?: string,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  freezeBalanceV2(
    amount: number,
    resource?: TransactionResource,
    address?: string,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  unfreezeBalanceV2(
    amount: number,
    resource?: TransactionResource,
    address?: string,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  cancelUnfreezeBalanceV2(address?: string, options?: TransactionOptions): Promise<TronTransaction>;

  delegateResource(
    amount: number,
    receiverAddress: string,
    resource?: TransactionResource,
    address?: string,
    lock?: boolean,
    lockPeriod?: number,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  undelegateResource(
    amount: number,
    receiverAddress: string,
    resource?: TransactionResource,
    address?: string,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  withdrawExpireUnfreeze(address?: string, options?: TransactionOptions): Promise<TronTransaction>;

  withdrawBlockRewards(address?: string, options?: TransactionOptions): Promise<TronTransaction>;

  applyForSR(address: string, url: string, options?: TransactionOptions): Promise<TronTransaction>;

  vote(votes?: Record<string, number>, voterAddress?: string, options?: TransactionOptions): Promise<TronTransaction>;

  createSmartContract(options?: ContractCreateOptions, issuerAddress?: string): Promise<TronTransaction>;

  triggerSmartContract(
    contractAddress: string,
    functionSelector: string,
    options?: MethodCallOptions | MethodSendOptions,
    parameters?: any[],
    issuerAddress?: string
  ): Promise<TronTransactionExtention>;

  triggerConstantContract(
    contractAddress: string,
    functionSelector: string,
    options?: MethodCallOptions,
    parameters?: any[],
    issuerAddress?: string
  ): Promise<TronTransactionExtention>;

  triggerConfirmedConstantContract(
    contractAddress: string,
    functionSelector: string,
    options?: MethodCallOptions,
    parameters?: any[],
    issuerAddress?: string
  ): Promise<TronTransactionExtention>;

  estimateEnergy(
    contractAddress: string,
    functionSelector: string,
    options?: MethodSendOptions,
    parameters?: any[],
    issuerAddress?: string
  ): Promise<TronEstimateEnergyMessage>;

  deployConstantContract(options: DeployConstantContractOptions): Promise<TronTransactionExtention>;

  clearABI(contractAddress: string, ownerAddress?: string, options?: TransactionOptions): Promise<TronTransaction>;

  updateBrokerage(brokerage: number, ownerAddress?: string, options?: TransactionOptions): Promise<TronTransaction>;

  createToken(options: CreateTokenOptions, issuerAddress?: string): Promise<TronTransaction>;

  createAccount(accountAddress: string, address?: string, options?: TransactionOptions): Promise<TronTransaction>;

  updateAccount(accountName: string, address?: string, options?: TransactionOptions): Promise<TronTransaction>;

  setAccountId(accountId: string, address?: string, options?: TransactionOptions): Promise<TronTransaction>;

  updateToken(options: UpdateTokenOptions, issuerAddress?: string): Promise<TronTransaction>;

  sendAsset(
    to: string,
    amount: number,
    tokenID: string,
    from?: string,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  purchaseAsset(
    issuerAddress: string,
    tokenID: string,
    amount: number,
    buyer?: string,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  createAsset(options: CreateTokenOptions, issuerAddress?: string): Promise<TronTransaction>;

  updateAsset(options: UpdateTokenOptions, issuerAddress?: string): Promise<TronTransaction>;

  createProposal(
    parameters: TronProposalParametersEntry[],
    issuerAddress?: string,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  deleteProposal(proposalID: number, issuerAddress?: string, options?: TransactionOptions): Promise<TronTransaction>;

  voteProposal(
    proposalID: number,
    isApproval?: boolean,
    voterAddress?: string,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  createTRXExchange(
    tokenName: string,
    tokenBalance: number,
    trxBalance: number,
    ownerAddress?: string,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  createTokenExchange(
    firstTokenName: string,
    firstTokenBalance: number,
    secondTokenName: string,
    secondTokenBalance: number,
    ownerAddress?: string,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  injectExchangeTokens(
    exchangeID: number,
    tokenName: string,
    tokenAmount: number,
    ownerAddress?: string,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  withdrawExchangeTokens(
    exchangeID: number,
    tokenName: string,
    tokenAmount: number,
    ownerAddress?: string,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  tradeExchangeTokens(
    exchangeID: number,
    tokenName: string,
    tokenAmountSold: number,
    tokenAmountExpected: number,
    ownerAddress?: string,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  updateSetting(
    contractAddress: string,
    userFeePercentage: number,
    ownerAddress?: string,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  updateEnergyLimit(
    contractAddress: string,
    originEnergyLimit: number,
    ownerAddress?: string,
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  checkPermissions(permissions?: Permissions, type?: TronPermissionPermissionType): boolean;

  updateAccountPermissions(
    ownerAddress?: string,
    ownerPermissions?: Permissions,
    witnessPermissions?: Permissions,
    activesPermissions?: Permissions | Permissions[],
    options?: TransactionOptions
  ): Promise<TronTransaction>;

  newTxID(transaction: TronTransaction, options?: NewTransactionIdentityOptions): Promise<TronTransactionExtention>;

  alterTransaction(transaction: TronTransaction, options?: AlterTransactionOptions): Promise<TronTransactionExtention>;

  extendExpiration(
    transaction: TronTransaction,
    extension: number,
    options?: NewTransactionIdentityOptions
  ): Promise<TronTransactionExtention>;

  addUpdateData(
    transaction: TronTransaction,
    data: string,
    dataFormat?: TransactionDataFormat,
    options?: NewTransactionIdentityOptions
  ): Promise<TronTransactionExtention>;
}

export default TransactionBuilder;
