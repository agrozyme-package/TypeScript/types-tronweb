import TronWeb from '../index';
import { PluginInterface, PluginOptions, PluginRegisterResult } from '../types';

export declare class Plugin {
  tronWeb: TronWeb;
  pluginNoOverride: string[];
  disablePlugins?: string[];
  pluginInterface?: PluginInterface | { (options: PluginOptions): PluginInterface };

  constructor(tronWeb: TronWeb, options?: PluginOptions);

  register<T extends Plugin = Plugin>(proto: typeof T, options: PluginOptions): PluginRegisterResult;
}

export default Plugin;
