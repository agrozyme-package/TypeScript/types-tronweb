import { JsonFragment, JsonFragmentType, Result } from '@ethersproject/abi';
import injectpromise from 'injectpromise';
import TronWeb from '../../index';
import { ContractBoundMethod, MethodDefaultOptions } from '../../types';
import Contract from './index';

export declare class Method {
  tronWeb: TronWeb;
  contract: Contract;

  abi: JsonFragment[];
  name: string;

  inputs: ReadonlyArray<JsonFragmentType>;
  outputs: ReadonlyArray<JsonFragmentType>;

  functionSelector: string;
  signature: string;
  injectPromise: ReturnType<typeof injectpromise>;

  defaultOptions: MethodDefaultOptions;
  onMethod: ContractBoundMethod;

  constructor(contract: Contract, abi: JsonFragment[]);

  decodeInput(data: string): Result;
}

export default Method;
