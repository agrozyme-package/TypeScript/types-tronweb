import { JsonFragment, Result } from '@ethersproject/abi';
import injectpromise from 'injectpromise';
import TronWeb from '../../index';
import {
  ContractBoundMethod,
  ContractCreateOptions,
  ContractEventCallback,
  ContractEventResult,
  EventOptions,
  EventRawOptions,
} from '../../types';
import Method from './method';

export declare class Contract {
  tronWeb: TronWeb;
  injectPromise: ReturnType<typeof injectpromise>;

  address: string | false;
  abi: JsonFragment[];

  eventListener: number | false;
  eventCallback: ContractEventCallback | false;

  bytecode: string | false;
  deployed: boolean;
  lastBlock: number | false;

  methods: Record<string, ContractBoundMethod>;
  methodInstances: Record<string, Method>;
  props: string[];

  constructor(tronWeb: TronWeb, abi?: JsonFragment[], address?: string);

  hasProperty(property: PropertyKey): boolean;

  loadAbi(abi: JsonFragment[]): void;

  decodeInput(data: string): { name: string; params: Result };

  new(options: ContractCreateOptions, privateKey?: string): Promise<Contract>;

  at(contractAddress: string): Promise<Contract>;

  events<T = any>(options?: EventRawOptions | EventOptions, callback?: ContractEventCallback<T>): ContractEventResult<T>;
}

export default Contract;

export * from './method';
