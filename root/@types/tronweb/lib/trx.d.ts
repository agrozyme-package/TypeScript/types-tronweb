import { BytesLike, TypedDataDomain } from 'ethers';
import injectpromise from 'injectpromise';
import TronWeb from '../index';
import Validator from '../paramValidator';
import {
  TronAccount,
  TronAccountResourceMessage,
  TronAssetIssueContract,
  TronBlock,
  TronCanDelegatedMaxSizeResponseMessage,
  TronCanWithdrawUnfreezeAmountResponseMessage,
  TronChainParametersChainParameter,
  TronDelegatedResourceAccountIndex,
  TronExchange,
  TronGetAvailableUnfreezeCountResponseMessage,
  TronNodeInfo,
  TronProposal,
  TronSmartContract,
  TronTransaction,
  TronTransactionApprovedList,
  TronTransactionInfo,
  TronTransactionSignWeight,
  TronWitness,
} from '../proto';
import {
  BlockID,
  ConfirmeOptions,
  GetDelegatedResourceResult,
  SendHexTransactionResult,
  SendTransactionOptions,
  SendTransactionResult,
  TransactionResource,
  TypedDataTypedTypes,
  TypedDataTypedValue,
} from '../types';
import Contract from './contract';

export declare class Trx {
  tronWeb: TronWeb;
  injectPromise: ReturnType<typeof injectpromise>;
  validator: Validator;
  cache: { contracts: Record<string, Contract> };

  constructor(tronWeb?: TronWeb);

  static verifySignature(message: string, address: string, signature: string, useTronHeader?: boolean): boolean;

  static verifyMessageV2(message: string, signature: string): string;

  static verifyTypedData(
    domain: TypedDataDomain,
    types: TypedDataTypedTypes,
    value: TypedDataTypedValue,
    signature: string,
    address: string
  ): boolean;

  static signString(message: string, privateKey: string, useTronHeader?: boolean): string;

  static signMessageV2(message: BytesLike, privateKey: string): string;

  static _signTypedData(
    domain: TypedDataDomain,
    types: TypedDataTypedTypes,
    value: TypedDataTypedValue,
    privateKey?: string
  ): Promise<string>;

  getCurrentBlock(): Promise<TronBlock>;
  getConfirmedCurrentBlock(): Promise<TronBlock>;

  getBlock(block: BlockID): Promise<TronBlock>;
  getBlockByHash(blockHash: string): Promise<TronBlock>;
  getBlockByNumber(blockID: number): Promise<TronBlock>;

  getBlockTransactionCount(block: BlockID): Promise<number>;

  getTransactionFromBlock(block: BlockID): Promise<TronTransaction[]>;
  getTransactionFromBlock(block: BlockID, index: number): Promise<TronTransaction>;

  getTransaction(transactionID: string): Promise<TronTransaction>;
  getConfirmedTransaction(transactionID: string): Promise<TronTransaction>;

  getUnconfirmedTransactionInfo(transactionID: string): Promise<TronTransactionInfo>;
  getTransactionInfo(transactionID: string): Promise<TronTransactionInfo>;

  //  Response HTTP Code 405 Method Not Allowed or blocked by CORS policy: No 'Access-Control-Allow-Origin'
  getTransactionsToAddress(address?: string, limit?: number, offset?: number): Promise<TronTransaction[]>;
  getTransactionsFromAddress(address?: string, limit?: number, offset?: number): Promise<TronTransaction[]>;

  getTransactionsRelated(
    address?: string,
    direction: 'all' | 'from' | 'to',
    limit?: number,
    offset?: number
  ): Promise<TronTransaction[]>;

  getAccount(address?: string): Promise<TronAccount>;

  // Unknown id
  getAccountById(id: string): Promise<TronAccount>;

  getAccountInfoById(id: string, options?: { confirmed?: boolean }, callback: Function): void;

  getBalance(address?: string): Promise<number>;

  getUnconfirmedAccount(address?: string): Promise<TronAccount>;

  // Unknown id
  getUnconfirmedAccountById(id: string): Promise<TronAccount>;

  getUnconfirmedBalance(address?: string): Promise<number>;

  getBandwidth(address?: string): Promise<number>;

  getTokensIssuedByAddress(address?: string): Promise<Record<string, TronAssetIssueContract>>;
  getTokenFromID(tokenID: string | number): Promise<TronAssetIssueContract>;

  listNodes(): Promise<string[]>;

  getBlockRange(start?: number, end?: number): Promise<TronBlock[]>;

  listSuperRepresentatives(): Promise<TronWitness[]>;

  listTokens(limit?: number, offset?: number): Promise<TronAssetIssueContract[]>;

  timeUntilNextVoteCycle(): Promise<number>;

  getContract(contractAddress: string): Promise<TronSmartContract>;

  verifyMessage(message: string, signature: string, address?: string, useTronHeader?: boolean): Promise<boolean>;

  verifyMessageV2(message: string, signature: string): Promise<string>;

  verifyTypedData(
    domain: TypedDataDomain,
    types: TypedDataTypedTypes,
    value: TypedDataTypedValue,
    signature: string,
    address?: string
  ): Promise<boolean>;

  sign(transaction: string, privateKey?: string, useTronHeader?: boolean, multisig?: boolean): Promise<string>;

  sign(
    transaction: TronTransaction,
    privateKey?: string,
    useTronHeader?: boolean,
    multisig?: boolean
  ): Promise<TronTransaction>;

  signMessageV2(message: BytesLike, privateKey?: string): Promise<string>;

  _signTypedData(
    domain: TypedDataDomain,
    types: TypedDataTypedTypes,
    value: TypedDataTypedValue,
    privateKey?: string
  ): Promise<string>;

  multiSign(transaction: TronTransaction, privateKey?: string, permissionId?: number): Promise<TronTransaction>;

  getApprovedList(transaction: TronTransaction): Promise<TronTransactionApprovedList>;

  getSignWeight(transaction: TronTransaction, permissionId: number): Promise<TronTransactionSignWeight>;

  sendRawTransaction(signedTransaction: TronTransaction): Promise<SendTransactionResult>;
  sendHexTransaction(signedHexTransaction: string): Promise<SendHexTransactionResult>;
  sendTransaction(to: string, amount: number, options?: SendTransactionOptions): Promise<SendTransactionResult>;

  sendToken(
    to: string,
    amount: number,
    tokenID: string | number,
    options?: SendTransactionOptions
  ): Promise<SendTransactionResult>;

  freezeBalance(
    amount: number,
    duration?: number,
    resource?: TransactionResource,
    options?: SendTransactionOptions,
    receiverAddress?: string
  ): Promise<SendTransactionResult>;

  unfreezeBalance(
    resource?: TransactionResource,
    options?: SendTransactionOptions,
    receiverAddress?: string
  ): Promise<SendTransactionResult>;

  updateAccount(accountName: string, options?: SendTransactionOptions): Promise<SendTransactionResult>;

  signMessage(transaction: string, privateKey?: string, useTronHeader?: boolean, multisig?: boolean): Promise<string>;

  signMessage(
    transaction: TronTransaction,
    privateKey?: string,
    useTronHeader?: boolean,
    multisig?: boolean
  ): Promise<TronTransaction>;

  sendAsset(
    to: string,
    amount: number,
    tokenID: string | number,
    options?: SendTransactionOptions
  ): Promise<SendTransactionResult>;

  send(to: string, amount: number, options?: SendTransactionOptions): Promise<SendTransactionResult>;

  sendTrx(to: string, amount: number, options?: SendTransactionOptions): Promise<SendTransactionResult>;

  broadcast(signedTransaction: TronTransaction): Promise<SendTransactionResult>;

  broadcastHex(signedHexTransaction: string): Promise<SendHexTransactionResult>;

  signTransaction(
    transaction: string,
    privateKey?: string,
    useTronHeader?: boolean,
    multisig?: boolean
  ): Promise<string>;

  signTransaction(
    transaction: TronTransaction,
    privateKey?: string,
    useTronHeader?: boolean,
    multisig?: boolean
  ): Promise<TronTransaction>;

  getProposal(proposalID: number): Promise<TronProposal>;

  listProposals(): Promise<TronProposal[]>;

  getChainParameters(): Promise<TronChainParametersChainParameter[]>;

  getAccountResources(address?: string): Promise<TronAccountResourceMessage>;

  getDelegatedResourceV2(
    fromAddress?: string,
    toAddress?: string,
    options?: ConfirmeOptions
  ): Promise<GetDelegatedResourceResult>;

  getDelegatedResourceAccountIndexV2(
    address?: string,
    options?: ConfirmeOptions
  ): Promise<TronDelegatedResourceAccountIndex>;

  getCanDelegatedMaxSize(
    address?: string,
    resource?: TransactionResource,
    options?: ConfirmeOptions
  ): Promise<TronCanDelegatedMaxSizeResponseMessage>;

  getAvailableUnfreezeCount(
    address?: string,
    options?: ConfirmeOptions
  ): Promise<TronGetAvailableUnfreezeCountResponseMessage>;

  getCanWithdrawUnfreezeAmount(
    address?: string,
    timestamp?: number,
    options?: ConfirmeOptions
  ): Promise<TronCanWithdrawUnfreezeAmountResponseMessage>;

  getExchangeByID(exchangeID: number): Promise<TronExchange>;
  listExchanges(): Promise<TronExchange[]>;
  listExchangesPaginated(limit?: number, offset?: number): Promise<TronExchange[]>;

  getNodeInfo(): Promise<TronNodeInfo>;

  getTokenListByName(tokenName: sting): Promise<TronAssetIssueContract[] | TronAssetIssueContract>;
  getTokenByID(tokenID: string | number): Promise<TronAssetIssueContract>;

  getReward(address?: string): Promise<number>;
  getUnconfirmedReward(address?: string): Promise<number>;

  getBrokerage(address?: string): Promise<number>;
  getUnconfirmedBrokerage(address?: string): Promise<number>;
}

export default Trx;
