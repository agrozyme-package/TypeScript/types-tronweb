import { AxiosInstance } from 'axios';
import { HttpProviderHeaders } from '../../types';

export declare class HttpProvider {
  host?: string;
  timeout: number;
  user: string | false;
  password: string | false;
  headers: HttpProviderHeaders;
  statusPage: string;
  instance?: AxiosInstance;

  constructor(
    host?: string,
    timeout?: number,
    user?: string,
    password?: string,
    headers?: HttpProviderHeaders,
    statusPage?: string
  );

  setStatusPage(statusPage?: string);

  isConnected(statusPage?: string): Promise<boolean>;

  request<T = any, D = any>(url: string, payload?: D, method?: string): Promise<T>;
}

export default HttpProvider;
