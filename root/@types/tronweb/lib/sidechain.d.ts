import { BytesLike } from 'ethers';
import injectpromise from 'injectpromise';
import TronWeb from '../index';
import Validator from '../paramValidator';
import { TronTransaction } from '../proto';
import { SideChainOptions } from '../types';
import utils from '../utils';

export declare class SideChain {
  mainchain: TronWeb;
  sidechain: TronWeb;
  utils: utils;
  injectPromise: ReturnType<typeof injectpromise>;
  validator: Validator;

  constructor<T extends TronWeb = TronWeb>(
    options: SideChainOptions,
    proto: typeof T,
    mainchain: TronWeb,
    privateKey?: string
  );

  isAddress(address?: string): boolean;

  setMainGatewayAddress(mainGatewayAddress: string);

  setSideGatewayAddress(sideGatewayAddress: string);

  setChainId(sideChainId: string);

  signTransaction(priKeyBytes: BytesLike, transaction: TronTransaction): TronTransaction;

  multiSign(transaction: TronTransaction, privateKey?: string, permissionId?: number): Promise<TronTransaction>;

  sign(
    transaction: TronTransaction,
    privateKey?: string,
    useTronHeader?: boolean,
    multisig?: boolean
  ): Promise<TronTransaction>;
}

export default SideChain;
