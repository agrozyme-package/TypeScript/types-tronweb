import injectpromise from 'injectpromise';
import TronWeb from '../index';
import { EventOptions, EventRawOptions, EventRawResult, EventResult, TronWebProvider } from '../types';

export declare class Event {
  tronWeb: TronWeb;
  injectPromise: ReturnType<typeof injectpromise>;

  constructor(tronWeb: TronWeb);
  setServer(eventServer?: TronWebProvider | string, healthcheck?: string);

  getEventsByContractAddress<T>(contractAddress: string, options: EventRawOptions): Promise<EventRawResult<T>[]>;
  getEventsByContractAddress<T>(contractAddress: string, options?: EventOptions): Promise<EventResult<T>[]>;

  getEventsByTransactionID<T>(transactionID: string, options: EventRawOptions): Promise<EventRawResult<T>[]>;
  getEventsByTransactionID<T>(transactionID: string, options?: EventOptions): Promise<EventResult<T>[]>;
}

export default Event;
