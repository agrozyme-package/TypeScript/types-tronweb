export type RequiredPick<T, K extends keyof T> = Required<Pick<T, K>> & Partial<T>;
export type PartialPick<T, K extends keyof T> = Partial<Pick<T, K>> & Omit<T, K>;
