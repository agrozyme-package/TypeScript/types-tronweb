import { Bytes, BytesLike } from 'ethers';

export declare function byte2hexStr(byte: number): string;

export declare function bytesToString(arr: BytesLike): string;

export declare function hextoString(hex: string): string;

export declare function byteArray2hexStr(byteArray: Bytes): string;

export declare function base64DecodeFromString(string64: string): Bytes;

export declare function base64EncodeToString(bytes: Bytes): string;
