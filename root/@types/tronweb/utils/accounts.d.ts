import { Account, AccountOptions, AccountWithMnemonic } from '../types';

export declare function generateAccount(): Account;

export declare function generateRandom(options?: AccountOptions): AccountWithMnemonic;

export declare function generateAccountWithMnemonic(
  mnemonic: string,
  path?: string,
  wordlist?: string
): AccountWithMnemonic;
