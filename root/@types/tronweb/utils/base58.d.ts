import { Bytes } from 'ethers';

export declare function encode58(buffer: Bytes): string;

export declare function decode58(string: string): Bytes;
