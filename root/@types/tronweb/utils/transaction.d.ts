import { TronPbftMessage, TronTransaction } from '../proto';

export interface TransactionProtobufArguments {
  Permission_id?: number;
  fee_limit?: number;
}

export interface TransactionProtobufOptions {
  data?: string;
  fee_limit?: number;
}

export declare function txJsonToPb(transaction: TronTransaction): TronPbftMessage;
export declare function txPbToTxID(transactionPb: TronPbftMessage): string;
export declare function txPbToRawDataHex(pb: TronPbftMessage): string;
export declare function txJsonToPbWithArgs(
  transaction: TronTransaction,
  args?: TransactionProtobufArguments,
  options?: TransactionProtobufOptions
): TronPbftMessage;
export declare function txCheckWithArgs(
  transaction: TronTransaction,
  args?: TransactionProtobufArguments,
  options?: TransactionProtobufOptions
): boolean;
export declare function txCheck(transaction: TronTransaction): boolean;
