import { JsonFragment } from '@ethersproject/abi';
import { EventRawResult, EventResult } from '../types';
import * as abi from './abi';
import * as accounts from './accounts';
import * as base58 from './base58';
import * as bytes from './bytes';
import * as code from './code';
import * as crypto from './crypto';
import * as ethersUtils from './ethersUtils';
import * as message from './message';
import * as transaction from './transaction';
import { TypedDataEncoder as _TypedDataEncoder } from './typedData';

interface Utils {
  isValidURL(url: any): boolean;
  isObject(obj: any): boolean;
  isArray(array: any): boolean;
  isJson(string: any): boolean;
  isBoolean(bool: any): boolean;
  isBigNumber(number: any): boolean;
  isString(string: any): boolean;
  isFunction(obj: any): boolean;
  isHex(string: any): boolean;
  isInteger(number: any): boolean;
  hasProperty(obj: Object, property: PropertyKey): boolean;
  hasProperties(obj: Object, ...properties: PropertyKey[]): boolean;
  mapEvent<T>(event: EventRawResult<T>): EventResult<Record<keyof T, string>>;
  parseEvent<T>(event: EventRawResult<T>, fragment: JsonFragment): EventRawResult<T>;
  padLeft(input: any, padding: string, amount: number): string;
  isNotNullOrUndefined(val: any): boolean;
  sleep(millis?: number): Promise<void>;
}

declare const utils: Utils;

export default {
  ...utils,
  code,
  accounts,
  base58,
  bytes,
  crypto,
  abi,
  message,
  _TypedDataEncoder,
  transaction,
  ethersUtils,
};
