import { Description } from '@ethersproject/properties';

export { checkResultErrors, Result } from 'ethers';
export { Interface, Indexed, LogDescription, TransactionDescription, ErrorFragment } from '@ethersproject/abi';

export declare class ErrorDescription extends Description<ErrorDescription> {
  readonly errorFragment: ErrorFragment;
  readonly name: string;
  readonly args: Result;
  readonly signature: string;
  readonly sighash: string;
}
