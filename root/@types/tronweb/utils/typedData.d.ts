import { TypedDataDomain } from 'ethers';
import {
  TypedDataPayload,
  TypedDataTypedEncoder,
  TypedDataTypedTypes,
  TypedDataTypedValue,
  TypedDataVisitCallback,
} from '../types';

export declare class TypedDataEncoder {
  readonly types: TypedDataTypedTypes;
  readonly primaryType: string;

  constructor(types: TypedDataTypedTypes);

  static from(types: TypedDataTypedTypes): TypedDataEncoder;

  static getPrimaryType(types: TypedDataTypedTypes): string;

  static hashStruct(name: string, types: TypedDataTypedTypes, value: TypedDataTypedValue): string;

  static hashDomain(domain: TypedDataDomain): string;

  static encode(domain: TypedDataDomain, types: TypedDataTypedTypes, value: TypedDataTypedValue): string;

  static hash(domain: TypedDataDomain, types: TypedDataTypedTypes, value: TypedDataTypedValue): string;

  static getPayload(domain: TypedDataDomain, types: TypedDataTypedTypes, value: TypedDataTypedValue): TypedDataPayload;

  getEncoder(type: string): TypedDataTypedEncoder;

  encodeType(name: string): string;

  encodeData(type: string, value: TypedDataTypedValue): string;

  hashStruct(name: string, value: TypedDataTypedValue): string;

  encode(value: TypedDataTypedValue): string;

  hash(value: TypedDataTypedValue): string;

  visit(value: TypedDataTypedValue, callback: TypedDataVisitCallback);
}
