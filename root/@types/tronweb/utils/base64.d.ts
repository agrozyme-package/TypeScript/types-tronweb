import { Bytes } from 'ethers';

export declare class Base64 {
  encode(input: string): string;

  encodeIgnoreUtf8(inputBytes: Bytes): string;

  decode(input: string): string;

  decodeToByteArray(input: string): Bytes;
}
