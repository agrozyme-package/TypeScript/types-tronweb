import { Bytes, BytesLike } from 'ethers';

export {
  base64DecodeFromString,
  base64EncodeToString,
  byte2hexStr,
  byteArray2hexStr,
  bytesToString,
  hextoString,
} from './bytes';

export declare function bin2String(array: BytesLike): string;

export declare function arrayEquals<T>(array1: ArrayLike<T>, array2: ArrayLike<T>, strict?: boolean);

export declare function stringToBytes(str: string): Bytes;

export declare function hexChar2byte(c: string): number;

export declare function isHexChar(c: any): number;

export declare function hexStr2byteArray(str: string, strict?: boolean): Bytes;

export declare function strToDate(str: string): Date;

export declare function isNumber(c: any): number;

export declare function getStringType(str: any): number;
