import { SignatureLike } from '@ethersproject/bytes';
import { BytesLike } from 'ethers/src.ts/utils/data';

export {
  keccak256,
  sha256,
  toUtf8Bytes,
  toUtf8String,
  recoverAddress,
  SigningKey,
  AbiCoder,
  Signature,
  concat,
  id,
  Mnemonic,
  Wordlist,
  wordlists,
  Wallet as ethersWallet,
  HDNodeWallet as ethersHDNodeWallet,
  getBytes,
  computeHmac,
} from 'ethers';

export { Interface } from './interface';

export declare function splitSignature(sigBytes: SignatureLike): Signature;
export declare function joinSignature(splitSig: SignatureLike): string;
export declare function arrayify(value: BytesLike): Uint8Array;
export declare const FormatTypes = {
  sighash: 'sighash',
  minimal: 'minimal',
  full: 'full',
  json: 'json',
};

export declare function isValidMnemonic(phrase: string, wordlist?: null | Wordlist): boolean;
