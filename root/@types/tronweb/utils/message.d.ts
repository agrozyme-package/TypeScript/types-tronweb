import { BytesLike, SignatureLike } from '@ethersproject/bytes';

export declare const TRON_MESSAGE_PREFIX: string;

export declare function hashMessage(message: BytesLike): string;

export declare function signMessage(message: BytesLike, privateKey: BytesLike): string;

export declare function verifyMessage(message: BytesLike, signature: SignatureLike): string;
