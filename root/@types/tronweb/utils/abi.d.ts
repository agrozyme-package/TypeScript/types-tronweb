import { JsonFragment, Result } from '@ethersproject/abi';

export declare function decodeParams(types: string[], output: string, ignoreMethodHash: boolean): Result;

export declare function decodeParams(
  names: string[],
  types: string[],
  output: string,
  ignoreMethodHash: boolean
): Result;

export declare function encodeParams(types: string[], values: any[]): string;

export declare function encodeParamsV2ByABI(funABI: JsonFragment, args: any[]): string;

export declare function decodeParamsV2ByABI(funABI: JsonFragment, data: any[]): Result;
