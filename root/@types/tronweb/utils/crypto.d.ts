import { Bytes, BytesLike } from '@ethersproject/bytes';
import { TronTransaction } from '../proto';

export declare function getBase58CheckAddress(addressBytes: Bytes): string;

export declare function decodeBase58Address(base58Sting: string): Bytes | false;

export declare function signTransaction(priKeyBytes: BytesLike, transaction: TronTransaction): TronTransaction;

export declare function arrayToBase64String(a: Bytes): string;

export declare function signBytes(privateKey: BytesLike, contents: Bytes): string;

export declare function _signTypedData(domain: object, types: object, value: BytesLike, privateKey: string);

export declare function getRowBytesFromTransactionBase64(base64Data: string);

export declare function genPriKey(): Bytes;

export declare function computeAddress(pubBytes: Bytes): Bytes;

export declare function getAddressFromPriKey(priKeyBytes: Bytes): Bytes;

export declare function decode58Check(addressStr: string): Bytes | false;

export declare function isAddressValid(base58Str: any): boolean;

export declare function getBase58CheckAddressFromPriKeyBase64String(priKeyBase64String: string): string;

export declare function getHexStrAddressFromPriKeyBase64String(priKeyBase64String: string): string;

export declare function getAddressFromPriKeyBase64String(priKeyBase64String: string): string;

export declare function getPubKeyFromPriKey(priKeyBytes: Bytes): Bytes;

export declare function ECKeySign(hashBytes: Bytes, priKeyBytes: Bytes): string;

export declare function SHA256(msgBytes: Bytes): Bytes;

export declare function passwordToAddress(password: string): string;

export declare function pkToAddress(privateKey: string, strict?: boolean): string;
