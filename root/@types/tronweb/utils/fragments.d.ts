export {
  ParamType,
  Fragment,
  ErrorFragment,
  EventFragment,
  ConstructorFragment,
  FunctionFragment,
} from '@ethersproject/abi';

export declare class NamedFragment extends Fragment {}
export declare class FallbackFragment extends Fragment {}
export declare class StructFragment extends Fragment {}
