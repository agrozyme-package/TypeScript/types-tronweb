export declare const ADDRESS_SIZE: number;
export declare const ADDRESS_PREFIX: string;
export declare const ADDRESS_PREFIX_BYTE: number;
export declare const ADDRESS_PREFIX_REGEX: RegExp;

export declare const TRON_BIP39_PATH_PREFIX: string;
export declare const TRON_BIP39_PATH_INDEX_0: string;
