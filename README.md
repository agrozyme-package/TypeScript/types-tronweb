# types-tronweb

TypeScript definitions for TronWeb

- Include all TronWeb and TronLink types from source code
- convert protobuf definitions to typescript definitions and re-export types with prefix `Tron`
- TronLink types with prefix `TronLink`
- only the TronWeb class can use `new TronWeb` by `import TronWeb from 'tronweb'`
- other declare class always use for type hint
- use this package, to create folder `@types/tronweb` under the source folder and put the code in `index.d.ts`

```ts
declare module 'tronweb' {
  export * from '@agrozyme/types-tronweb';
  import TronWeb from '@agrozyme/types-tronweb';
  export default TronWeb;
}
```

- In browser project, it can manually define the `TronLink` injected object type

```ts
declare global {
  interface Window {
    tron?: Tron;
    tronLink?: TronLink;
    tronWeb?: TronWeb;
  }
}
```
